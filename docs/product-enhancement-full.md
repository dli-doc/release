---
search:
  exclude: true
---

# Product enhancement guide

## Administration

### Transaction Information in Attendance Regularization Email

#### Jan 2020 -  #16481

The client required transaction request information to list in the Attendance Regularization email alert. At present, only minimal information was shared in the email. 

Included transaction details (Date, Time, Transaction Type, Remarks) for Absent, Missing In/OUT and Break Permission

-   Missing IN/Missing Out - List the newly added transaction with datetime, transaction type and Comments

-   Absent - Display the In and Out information with datetime, transaction type and Comments

-   Break Permission - List the Break From and Break To data, include the break duration and comments

![](/img/product-enhancement/image1.png)

### Alert on Annual Leave balance threshold

#### Jan 2020 -  #16643

The client required a system to send alert notification mail to employees when his/her accrual get more than or equal to 40 days, mentioning that his/her accrual leave will expires if leave balance exceeds 45 and therefore to plan the vacation accordingly.

To implement the feature, execute two scripts to enable the value types of new Alert type and Day selection on screen. Title for Email Alert can be customised using XML

![](/img/product-enhancement/image2.png)

### Email Template For Attendance Violation Report-Department Wise

#### Nov 2019 -  #16464

The client wants to send the report format as an email attachment. The attachment must be sent to the managers/HR with the data of their respective subordinates.

To implement the change, follow the steps below:

1.  Execute the new REPX created for Attendance Violation Summary Report - Department Wise.

2.  Create custom report by using the REPX and select the same in Email Settings screen

3.  Execute the script for selecting Custom Report in Email settings.

4.  Execute the script for creating/importing Custom Report in Custom Report Designer.

5.  The Email Settings can be configured as required.

![](/img/product-enhancement/image3.png)

### Email Template For Attendance Violation Report-Employee Wise

#### Nov 2019 -  #16465

The client requires to send the attached report format as an email attachment. The attachment must be sent to the managers/HR with the data of their respective subordinates.

To implement the change, follow the steps below:

1.  Execute the new REPX was created for Attendance Violation Report-Employee Wise

2.  Execute the script to get violation report data from Consolidated Attendance Report.

3.  Create a custom report using the REPX and select the same in Email Settings screen.

4.  Execute the script for getting the custom report for selection.

5.  Execute the script for selecting Consolidated Report in Custom Report Designer.

6.  The email settings can be configured as per the requirement.

![](/img/product-enhancement/image4.png)

### Option to Change the Email Sending Day

#### Sep 2019 -  #16326

Currently, the previous month attendance report is sent to the employees only if the Email Sending Day is set to 1st day of the current month. Now, the client required a provision to select any day within the existing month to send the previous month report. Similarly, to send the previous week attendance data (Sun-Sat), currently, the sending day must be set to Sunday. Provision to set this to any day within the week is provided.

Execute a script to enable the changes.

![](/img/product-enhancement/image5.png)

### Option restrict duplication of MOLID and IBAN

#### Sep 2019 -  #16090

The client required an option to restrict the duplication assignment of same MOLID and IBAN for employees.

This customisation is added by introducing a new column in the Card Type for Bank Parameter linked at Employee level to Data Type String.

![](/img/product-enhancement/image6.png)

### Dependent Age Limit Notification

#### Aug 2019 -  #14721

An alert notification was introduced in the system which will generate alert to the user when the age limit of the dependants exceeds 18 years.

For implementing this feature execute a script to enable Ageing alert type.

![](/img/product-enhancement/image7.PNG)

### Missing document Notification to employee and HR

#### June 2019 -  #14555

The client required to send notification to HR and employees in case of missing documents.

For implementing this the following steps were followed.

1.  Added a new category named "Missing Personal Record Notification" in Email settings.

2.  Execute the REPX file for the Custom Report.

3.  Run three scripts for displaying the custom report created in the email settings and for displaying the new category created in the email settings.

     ![](/img/product-enhancement/image8.png)

*Fig: Email settings*

### Option to copy master and application details from existing company

#### Dec 2018 -  # 9976

The client wants to clone the following items (data, fields, settings, etc.) from an existing company to the new company they are creating.

-   **Data, fields and settings to be cloned**: card types, card master, sequence control, action form, application parameters etc.

For this option to appear in HR Works, two different scripts have to be executed in the HR Works database.

1.  Script to create **Company Creation** menu. This will insert a new screen (Company Creation) in the **Administration** module >> **System Management** section.

2.  Script to create new value types (screen selections)

Once the above scripts are executed, can make a clone of the existing company from the **Compony Creation** screen. From the **Data to Copy** dropdown field of the **Company Creation** screen, users can select the items they want to copy from the existing company to the new company they are creating.
![](/img/product-enhancement/image9.png)

On successful completion of the company creation process, the following message appears:
![](/img/product-enhancement/image10.png)

## Payroll Management

### P9 form in HRWorks

#### April 2021 -  #21914

P9 form was introduced in the HRWorks.

In order to implement the new feature, follow the steps below.

1.  Execute a query for enabling the menu.

2.  Execute an SQL file.

![](/img/product-enhancement/image11.png)

### Monthly incentive for Employee

#### July 2021 -  #21935

The client required a provision to automate the current Incentive calculation in the Payroll module. The monthly incentive of each employee must be calculated based on the monthly target assigned to employee against total units completed.
Parameters for the incentive calculation is mentioned below which will be accepted at the employee level against each department.
1. Unit rate agreed with the employee

2. Daily target of an employee

3. Effective date

The following information will be entered by the HR on monthly basis,

1.  Total Units completed by the employee for the month

2. No of days worked

System calculates the Employee Incentive for the month after providing the inputs by using the logic below

Target for the month = No of days worked  Daily target\
Payable Incentive = (Total Units completed -- Target for the month)  Unit rate

Note: Unit rate agreed with the employee and Daily target of the employee will be updated once in a month with effective date. System will consider the latest value as per the effective date.

In order to implement the feature, follow the steps below:

Execute a script to enable the new Entry Types

Execute an SP

![](/img/product-enhancement/image12.png)

### Time sheet validation and creation with exception report

#### Jan 2021 -  #20884

The client required to integrate the time sheet data from ERP to HRW. Moreover, the Timesheet data must be validated based on the Attendance in HRW before creating timesheet. An exception report was also required after completing the validation for the selected period.

In order to implement the feature, follow the steps below:

-   Execute the script to enable parameter, menu and value type.

-   Execute scripts to create payroll table and views.

-   Execute scripts to create company tables and views.

-   The parameter 'Default Time Sheet from TA' to be set as Yes to take attendance data.

-   The batch creation is implemented using DLIHRMail service.

![](/img/product-enhancement/image13.png)

### Change in HRA Calculation

#### Feb 2020 -  #17367

The client required some changes in the HRA calculation. The system has to pay full HRA amount to employee, who has worked on wage type and who at least worked one day in a month. HRA amount will be fixed for one month.

For example, consider that HRA for employee is 500 QAR.

If an employee worked on wage type for whole days in a month, the system has to pay 500 QAR as HRA. If an employee worked on wage type for 1 day (Remaining days as contract), the system has to pay 500 QAR as HRA.

In order to implement the change, set PayCodeID of HRA in custom SP and execute a query[.]{.underline}

![](/img/product-enhancement/image14.PNG)

### Split Time sheet entry or Upload

#### Feb 2020 -  #18184

The client required the provision for users to enter the Time sheet for only selected days or upload TS only for selected days in Wage type-based timesheet. An employee cannot have two batches with same date.

Execute a script to enable the parameter 'Monthly Wage Timesheet Entry' in TS-1 tab.

![](/img/product-enhancement/image15.png)

![C:\Users\\USER_2\\Desktop\\2020-01-27_11-54-09.png](/img/product-enhancement/image16.png)

### Document upload option in the Earning deduction batch screen

#### Jan 2020 -  #17530

The client required to upload documents along with Earning deduction batch entry. So an option was introduced to upload document while entering the earning deduction batch through the earning deduction batch screen. User can upload document by batch wise instead of employee wise. User will be able to view/ download each employee uploaded document via earning deduction batch report screen.

Execute a query to implement the change.

![](/img/product-enhancement/image17.png)

### Employee integration

#### Nov 2019 -  #17355

Employee details needs to be integrated to the staging named  HRW_AXEMPLOYEE_STAGING when an employee is hired, Employee data is been updated and employee is been separated.

To implement the change, follow the steps below:

1.  Execute the SP for integrating project as per the requirements

2.  Execute two queries to add a column named termination date column and to increase the size of the sponsor company column.

### GL Integration to staging table

#### Nov 2019 -  #16908

The client required to write GL entries to a staging table, using HRW standard screen and batch selection options. The features included in the screen are pointed below

-   Need to write CGS GL entries to a staging table, using HRW standard screen and batch selection options.

-   Need an option to see the status of batches, to see whether it has been interfaced or not (from the GL interface screen, against each batch)

-   While closing the payroll period, need to check whether all batches has been interfaced or not.

To implement the changes, follow the steps below.

1.  Execute two xml.

2.  Execute two SP

### Loan Eligibility Validation

#### Nov 2019 -  #17190

The client required the below pinpointed validations for loans in HR Works and HR Works Plus.

-   Employee could apply for HRADV only for first 1 year from joining date. After 1 year employee will not be eligible.

-   Employees can apply only for one loan per year.

-   An employee is eligible to apply for salary advance only after one year

Execute the SP for implementing the change.

![](/img/product-enhancement/image18.png)

### Project Integration

#### Nov 2019 -  #17356

New projects created in AX shall push the details into staging table  HRW_AXPROJECT_STAGING . Any new project update in the table needs to be updated to Project master in Position entity. Based on the company the data needs to be updated into respective company. Once the project is created in the company the status needs to be updated as  True .

To implement the change, follow the steps below:

1.  Execute the SP for integrating project as per the requirements

2.  Execute the query to add a column name identity column to the client db.

### Qatar WPS

#### Nov 2019 -  #17270

The client required to generate the WPS file according to the bank code details.

To implement the change, follow the steps below:

1.  Execute three view templates.

2.  Execute xml file for bank transfer file.

![](/img/product-enhancement/image19.png)

### Salary revision through Web service Utility

#### Oct 2019 -  #13841

Currently if an employee has Basic, HRA and GOSI accrual Basis Amount then once there is a salary change coming from web services, then GOSI Accrual basis amount must be considered.

To implement the change, salary revision utility was changed.

Mention the pay code that must be considered in the salary revision in the XML file.

### To remove showing of separated employees from current payroll

#### Aug 2019 -  #15860

A new feature to avoid listing of the employees in the monthly payroll sheet even if the employees are separated with a future date.

This feature is achieved through the introduction of a new parameter in the PRL-1 tab of application parameter with name 'Block Payrollsheet For Separated Employees'. Execute a query to enable the application parameter.

![](/img/product-enhancement/image20.png)

### Validation of Basic Pay with Deductions

#### Aug 2019 -  #14705

In payroll calculation a new validation was introduced which validate the deduction of an employee's salary. The total deduction must not be more than 25% of the basic pay. If the deduction amount is more than 25% of basic pay including attendance related deductions like Absent, shortage etc then the system should generate an exception report of such employees.

- Execute the script.

    A custom stored procedure was added to get validation. Validation limit is set in the customised stored procedure. Default action for the validation will be 'Do Not Process Payroll' with an option to 'Continue Processing Payroll' 
    
    ![](/img/product-enhancement/image21.png)

### Validation for TS on payroll process

#### July 2019 -  # 15852

A new validation was added in the payroll processing. If daily time sheet of the employees is missing for any day, then the payroll for the employee must be in hold.

![](/img/product-enhancement/image22.png)

*Figure: Validation shown while processing payroll*

### Balance Compensatory Off need to payout during Final Settlement

#### June 2019 -  # 14875

The client required to payout the balance compensatory off to the employee when an employee resign. Following steps are executed to implement this.

In Application Parameter, LV-1 Tab, a new parameter named "Compensatory Settlement Paycode" was introduced.

Execute a script to enable the parameter.

![](/img/product-enhancement/image23.png)

*Fig: PAF Screen showing Settle Outstanding Compensatory Off during separation*

### Loan Amount limit based on employee service

#### May 2019 -  # 14551

The client required to add a validation message in the Loan Application screen to limit the loan amount based on the service period of the employees.

Execute an SP and set the Entity ID for bringing the validation message in the Loan Application Screen.

![](/img/product-enhancement/image24.png)

*Fig: Loan Application Screen showing the validation message*

### Approved OT to Pay through Off cycle Payroll

#### May 2019 -  # 14550

Initially OT payments were processed through the monthly payroll sheet. Now the client required to approve the OT payments through the Off cycle Payroll.

For this a new parameter was introduced in the Application Parameter, named 'OT Integration From ESS' in the ESSTS-1 tab in the HRW + Application Parameters. The value of the Parameter must be set as 'OT Entry (Off cycle)'.

Execute a query for enabling the Application Parameter.

![](/img/product-enhancement/image25.png)

*Figure: HRW + Application Parameter Screen*

### Option to restrict payroll period visibility in the final settlement screen

#### May 2019 -  # 14880

Currently in the Final Settlement Screen, pay period combo box list all the pay period starting from the system. The client required to restrict the visibility of the payroll period in the Final Settlement Screen.

For this a new parameter called 'Backdated Periods For Final Settlement' was introduced in the PRL-1 tab in the Application Parameter.

This restrict the payroll period visibility based on a flag in the system. By default, it lists all the pay periods Or else the payroll visibility will be based on the number set in the Application Parameter.

![](/img/product-enhancement/image26.png)

### Option to correct Attendance data from Payroll Module

#### April -2019 -  #14605

In HR Works payroll module, the details regarding the attendance such as overtime hours, shortage hours etc was not able to edit and the information from the attendance module was considered. Hence the client required to add a feature which enables to edit the attendance data if required from the payroll module and thus to calculate the payroll accurately.

In order to achieve this, a new parameter is added in the PRL-1 Tab named OT Entry Batch for TAM Interface Employees.

The new parameter contains three options which are

1. Do not Allow - OT from Overtime Entry Batch will not be considered in payroll calculation.

2. Add OT Batch OT to TAM OT - OT from Overtime Entry Batch will be added to OT from attendance data.

3. Override TAM OT with OT Batch OT - OT from attendance will not be considered if OT entry exist in Overtime entry batch.

![](/img/product-enhancement/image27.png)

*Fig: Application Parameter settings for Attendance correction from Payroll Module*

### Separation of employees without salary information

#### Mar 2019 -  # 14441

Some of the clients want to separate the employees without completing the Payroll process and Final Settlement. In such cases Termination Date is updated at the time of employee separation approval.

In order to implement this, following changes are made.

In Application Parameter, in HRW-1 tab, a new value named "Set Separation Date in Employee Master" was introduced with two options such as approval of Final Settlement and On Separation Action Form Approval if no Payroll Sheet exists for Employee.

While selecting On Separation Action Form Approval if no Payroll Sheet exists for Employee from the Application Parameter, it will check whether the employees in the employee separation Batch already have a payroll sheet and if not, it will update the Termination date or else will work as usual as the final settlement.

![](/img/product-enhancement/image28.png)

*Fig: Application Parameter Settings*

### OT Report Template

#### March-2019 -  # 13897

An OT report was required for the client in the Wage type Based Timesheet report screen.

For these two additional options was added in the Summary options in the Wage Type screen named as OT Sheet and OT Summary.

For this execute a script for getting the OT options.

Only one could be selected from OT Sheet and OT Summary at once during a specific month.

The OT Sheet report will show the complete OT Data during the selected month with displaying the data of each day, whereas OT Summary shows the summarised data of the OT details.

![](/img/product-enhancement/image29.png)

Fig: Wage Type Based Timesheet report

### Customization on Salary Hold and Release

#### Feb 2019 -  # 11755

In Salary Hold and Release, initially the total salary of an employee was hold till the month where the release hold is applied. It was not able to hold a Lumpsum amount or percentage of the employee salary for a particular month.

The client needed the following changes in the Salary Hold and Release.

1.  To hold the Lumpsum amount or Percentage of Net amount of the salary of a particular month.

2.  To release the amount either through monthly payroll or off cycle payroll.

- **In order to implement this, following changes are made.**

1.  In the **Salary Hold and Release** screen, a new option to select the Hold period is added.

2.  A query is executed in the DB.

    - **Salary Hold:**

        * While selecting the Batch Type as Hold Salary, a new check box and Hold period is displayed.

        * For Holding the salary for a particular period. Click the check box and enter the hold period.

    - **Salary Release:**
        * While selecting the Batch Type as Release Hold, a new option to select the Release method will be displayed.

        * Select either Normal Payroll or Off cycle Payroll as the Release method.

    - Further, release the batches, through Off cycle payroll, if the payment option is Off cycle. Or else process the settlement through normal Payroll sheet.

        ![](/img/product-enhancement/image30.png)

        *Figure: Salary Hold in Salary Hold and Release*

### Upload option in Cost Allocation Setup

#### Oct 2018 (#11912)

Cost Allocation transaction did not have **Upload data from Excel** option. This option has been added to the cost allocation transaction screen with the following features.

-   Generate Excel Template: As with other transactions with Excel upload option, cost allocation transaction also requires that an excel template be generated into which users can add cost allocation details of employees and then upload it. The generated excel template will have separate columns for every project so that the users can enter project-wise cost allocation data. Note that Project is one of the positional entities in the client DB.

-   Upload from Excel: Users can enter employee's cost allocation details into the generated excel template and then upload it to proceed with rest of the transaction process.

### Loan Repayment Installments Amount Deduction during Leave Settlement

#### July 2018 (#10375)

Usually, when an employee goes for leave, his loan repayment amount for leave start month is only deducted while processing leave settlement. But now, with this new feature, the loan repayment amount can be deducted for not only the leave start month but also for leave end month as per settings done in Application Parameter. Furthermore, there is an additional functionality where if the days in next month falls less than 15 days, then the deduction is not taken.

In order to achieve this functionality, a new parameter **Loan Installment Deductions in Leave Settlement** is added in the **Application Parameter- LOAN-1** tab. It consists of three options as:

<table class="TFtable" border="1">
  <tr>
       <th>Sl No.</th>
       <th>Parameter</th>
        <th>Description</th>
     </tr>
   <tr>
      <td>
         <p></p>
      </td>
      <td>
         <p>Loan Deductions till Payroll End Date of Leave Start Date </p>
      </td>
      <td>
         <p>By default, the parameter is set to ‘Loan Deductions till Payroll End Date of Leave Start Date’. Loan deduction in leave settlement works as normal in this case.</p>
         <p></p>
         <p>For instance, if an employee is going for leave from May 15<sup>th</sup> to June 15<sup>th</sup>, then if this parameter is set, then the loan repayment amount for the month of May only is deducted.</p>
         <p></p>
      </td>
   </tr>
   <tr>
      <td>
         <p></p>
      </td>
      <td>
         <p>Loan Deductions till Payroll End Date of Leave To Date</p>
      </td>
      <td>
         <p>Loan amount is deducted till the payroll end date of the ‘Leave to date’ month.</p>
         <p></p>
         <p>For instance, if an employee is going for leave from May 15<sup>th</sup> to June 15<sup>th</sup>, then if this parameter is set, then the loan repayment amount for the month of May and June is deducted.</p>
         <p></p>
      </td>
   </tr>
   <tr>
      <td>
         <p></p>
      </td>
      <td>
         <p>Loan Deductions till Payroll End Date of Leave To Date (Cutoff 15th)</p>
      </td>
      <td>
         <p>Loan amount is deducted till the payroll end date month of the Leave to date only if the settlement to date falls after 15th day of the Leave to date. </p>
         <p></p>
         <p>In this case, if the ‘Leave to date’ falls within 15<sup>th</sup> date of the next month, loan amount is deducted only for previous month.</p>
         <p></p>
         <p>For instance, if an employee is going for leave from May 15<sup>th</sup> to June 15<sup>th</sup>, then if this parameter is set, then the loan repayment amount for the month of May will be deducted and if the leave to date falls below 15<sup>th</sup> of next month, then the amount is not deducted for the month of June.</p>
      </td>
   </tr>
</table>

The deducting of loan repayment amount while performing leave settlement will be done on the basis of the value assigned for this parameter.
![](/img/product-enhancement/image31.png)

## Leave and Accrual Management

### Leave Return for continuous leave

#### April 2021 -  #21699

The client require to update the Leave return of the last leave application for any continuous leave applications instead of applying return date for individual application. For example if the employee has got an AL followed by UP then when user returns from Leave user will update return date for the UP leave only and return date for AL needs to be updated automatically. The change is applied in both HRW and ESS.

In order to implement the change, execute a query to enable the parameter.

![](/img/product-enhancement/image32.png)

![](/img/product-enhancement/image33.png)

### Accrual summary report

#### Jan 2021 -  #20346

Reports for indemnity accrual summary report and air ticket accrual summary report were developed.

In order to implement the change, execute the query.

![](/img/product-enhancement/image34.png)

### Generate a single report to show leaves from HR Worksand ESS

#### Feb 2020 -  #17451

The client required to generate a single leave report from HR Workswhich include all leaves applied from ESS, HRWorks, delay leave and Absent leave uploaded through time sheet.

In order to implement the change, execute an xml file.

![](/img/product-enhancement/image35.png)

### Provide public holiday salary through the leave settlement

#### Jan 2020 -  #15231

The client required some changes in the vacation pay calculation.

For implementing the change a new application parameter was added in Vacation Scheme in Vacation Scheme Details tab as 'Settlement Amount Calculation For', with options "Leave Days" and "No.of Days between Leave Dates".

While selecting 'Leave Days', normal leave settlement calculation is done based on vacation pay corresponding leave days and while selecting 'No. of Days between Leave Dates', vacation pay corresponding to No. of Days between Leave Date is considered.

Execute a query to enable the application parameter.

![](/img/product-enhancement/image36.png)

### Final Settlement changes

#### Dec 2019 -  #17782

The client required to view the Sponsor Company of employee instead of company name in the report.

To implement the changes, follow the steps below.

1.  Execute an REPX file

2.  Execute an XML file.

![](/img/product-enhancement/image37.png)

### Leave Return notification

#### Dec 2019 -  #16705

The client required to send the notification to Line Manager and HR on employee to be returning next day and today. Employee without Leave return date only need to be considered.

To implement the changes, follow the steps below.

1.  Execute a script to enable the alert type.

2.  Execute a script to enable the value type of new alert.

![](/img/product-enhancement/image38.png)

### Final Settlement Changes

#### Nov 2019 -  #17431

The following changes are introduced in the final settlement report.

-   Designation, Department, Category and Nationality was added between the service details and salary details

-   Salary transfer letter yes/no are linked to employee which is shown in between the service details and salary details.

-   Air ticket allowance is shown twice in the report.

To implement the change, follow the steps below:

1.  Execute the REPX file

2.  Execute the XML file

![](/img/product-enhancement/image39.png)

### Option for Summary report in Indemnity Accrual

#### Sep 2019 -  #16227

The client required the provision to take the print out of the Indemnity accruals summary and an option to group the report as per the entity.

Execute an xml to implement the changes.

![](/img/product-enhancement/image40.png)

### Option for Summary report in Ticket Accrual

#### Sep 2019 -  #16226

The client required the provision to take the print out of the ticket accruals summary and an option to group the report as per the entity.

Execute an xml to implement the changes.

![](/img/product-enhancement/image41.png)

### Option for Summary report in Vacation Accrual

#### Sep 2019 -  #16228

The provision to take the print out of the vacation accruals summary and an option to group the report as per the entity was introduced in HRWorks.

Execute an xml to implement the changes.

![](/img/product-enhancement/image42.png)

### Benefit Claim Request Change

#### Aug 2019 -  #15138

Currently in HR Works, it is possible to avail amount which is more than the eligible amount in a year. Therefore, the client required to restrict the amount as per his/her eligibility. Moreover, if the amount is not fully utilised, the balance must be carry forwarded to next year.

For implementing this,

1.  Execute a query

2.  New application parameter named 'Air Ticket Lumpsum amount' was added in the TKT-1 tab

3.  New option named 'Unlimited With Annual Limit Amount' was added in the ticket frequency parameter in TKT-1 tab.

![](/img/product-enhancement/image43.png)

### Option to avail the ticket accrued till last working day during Final Settlement

#### Aug 2019 -  #15581

Currently, during the final settlement, the system has option to avail ticket that accrued till last payroll. Now the client required an option to pay out full ticket balance, that employee have accrued till the last day.

For implementing this change follow the steps below:

1.  New options were added for the parameter 'Max. Request Ticket for Claim Type - Air Ticket'. Execute a query to enable newly added options.

2.  New Column 'Balance As On' is added in Air ticket availment screen.

![](/img/product-enhancement/image44.png)

### Year End leave balance in Vacation accrual report

#### July 2019 -  # 15626

The client required to add additional field in the Vacation Accrual Report named Pending Leaves and Year End Leave balance.

Pending leave displays the leave that need to be deducted for the year.

Year End Leave Balance displays the balance leave for the selected year.

For implementing the change, execute a SQL query.

In the RPT-1 Tab of Application parameter, Set the value for 'Additional fields in Vacation Accrual Report'.

![](/img/product-enhancement/image45.png)

*Figure: Vacation Accrual Report showing Pending leaves and Year end leave balance*

### Training cost recovery through Final Settlement

#### June 2019 -  # 14554

The client required to deduct the training cost during the final settlement. The training provided to the employees are tracked in the HR Works under the Personal Records. When an employee resigns before one year of training taken date, the amount of training cost is deducted during the employee's final settlement.

The training details stored in the HR Works are Effective Date, Training Type, Training subject and Training cost.

In order to implement the feature, execute the custom SP.

### Show Leave From Date and Leave To Date in the leave settlement screen

#### May 2019 -  # 15232

Initially the Leave Settlement Screen did not showcase the From Date and To Date information regarding the leave. Therefore, the client required to include the Leave From Date and Leave To Date in the Leave Settlement Screen.

![](/img/product-enhancement/image46.png)

*Fig: Leave Settlement screen*

### Two levels of approval during leave settlement.

#### Feb 2019 -  # 12491

In HR Works, there was a parameter (Multilevel AWF) by which users can set approval workflow for all common transactions.

The client requires a different workflow to be set for leave settlement transaction. For this a new parameter ' M**ultilevel AWF on Leave Settlement** ' is added in Application parameter AWF1 Tab.

### Air ticket encashment report

#### Feb-2019 -  # 13263

Initially, air ticket encashment report was not generated.

The client required to avail the Air ticket encashment report through the pay slip.

For implementing this change, import REPX file and execute XML file.

For creating the Air Ticket Encashment report, follow the steps below:

1.  In the Air Ticket Availment screen, Select the 'Off Cycle Payroll' as the Availment Option. This will allow to complete the availment process through the Off-cycle screen.

2.  While printing Pay slip select the report type as Air ticket Encashment and select Type as Off Cycle.

![](/img/product-enhancement/image47.png)

*Figure: Payslip screen*

### Vacation eligibility based on service days.

#### Dec 2018 -  # 12943

For the client, for a few employees, vacation days or eligibility will change as per the service days of employee.

Below lists the slab for accrual.

-   0-3 Years \- 22 working Days

-   3-6 Years \- 24 working Days

-   6-10 Years \- 26 working Days

-   10-20 Years \- 28 working Days

20 Years and above \- 30 working Days

Leave Days will be accrued on Pro-data basis, which means if an employee's slab changes at the mid of a month. His/ her balance should be calculated pro-data basis.

**Example**: If an employee completes the 3 years by 15th of June, his leave accrual should be 15 days with 22 working days and rest with 24 working Days.

In both ESS and HR Works Leave application, leave balance need to show as per the above accrual method. The client needs to show the leave balance as of leave start date

For this, a new parameter (**Apply Prorata Calculation in Vacation**) is added to **Application Parameter \>\>LV-1**. If the value of this parameter is set to 'Yes', leave days will be accrued based on pro-data basis. Leave balance in leave application will be shown as per this accrual method.

### Leave Settlement pay on Calendar days

#### Dec 2018 -  # 11866

Vacation pay is calculated based on the calculation criteria set in vacation codes alone. The client requires that it should be calculated based on the daily rate policy applicable to the employee.

To customize the system as per client's requirement, a new application parameter (**Leave Advance Pay Calculation**) is added to the **LV-1** tab of the **Application Parameter**. If its value is set as **Daily Rate Basis Till Leave To Date**, then the vacation pay calculation in leave settlement will change and new calculation will be as follows:

Vacation pay = sum (salary for pay components in vacation pay from leave start period to leave to date) - sum (salary for pay components in vacation pay from leave start period to leave from date)

### Leave settlement Recovery changes

####  Oct 2018 (#11068)

In HR Works, when an employee goes on vacation and requests for Leave salary advance, he/ she will be paid the salary till the leave start date and leave salary for the leave days. The leave salary consists of gross salary excluding COLA, OTHER and MOBILE allowance. These allowances will be paid for the leave days along with month end payroll. Leave salary paid to the employee will be considered as leave salary advance and it will be recovered from the subsequent payrolls. 

In case if an employee has got any additional payments such as OT, Bonus or allowances (COLA, OTHER and MOBILE), those payments will be made only after recovering the leave salary advance paid to the employee. The client now requires that the recovery be made only from the paid leave salary components and if any additional payments are there, they should not be touched and have to be paid as usual.

Example:

**Employee Salary**- Basic: 500, HRA: 200, TRA: 50, OTH: 50

**Leave** -- 16th May 2018 to 19 June 2018 (25 days)

**Off days** -- FRI-SAT

{%
   include-markdown "hrw/leave/table1.md"
   heading-offset=1
%}

**Summary of Changes**

The following changes are made

1.  **Application Parameter\
    **A new parameter '**Enable Pay Codewise Leave Advance Recovery Setup**' is added in the **LV-1** tab of the **Application Parameter** screen. Default value will be '**No'**. In order to meet client requirement and to enable the **Pay Pay Code-wise Leave Advance Recovery Setup** tab, the parameter value should be set to Yes.
    ![](/img/product-enhancement/image48.png)

2.  Vacation Codes screen:
    The **Pay Code-wise Leave Advance Recovery Setup** tab displays the following:

    1.  All payable earning codes which are not a part of Accrual Components will be displayed

    2.  An additional component named 'Salary Arrear'.

- For each earning code, any of the following Recovery Amount options can be set

1.  **Full Amount** (this will be the default option. If this is selected, then the full amount corresponding to the pay code will be subject to recovery).

2.  **Amount up to the amount Paid in Leave Settlement.** (if this option is selected then only the amount paid in the corresponding leave settlement will be subject to recovery)\
    ![](/img/product-enhancement/image49.png)

**Note**: The **Pay Codewise Leave Advance Recovery Setup** tab will appear only if the ***Recovery Amount*** options are active in the db.

### Vacation Availment Based on Accrual Average

#### **Oct 2018 (#10176)**

Currently the daily rate calculation method for vacation encashment is based on the **Daily Rate Calculation Basis** selection on the **Vacation Scheme** menu. The client now requires an option to set daily rate calculation method for vacation encashment based on vacation accrual average (ie Daily rate = total accrual amount/ total accrual days). For this a new dropdown field (**Vacation Encashment Calculation Method**) is added to the **Vacation Scheme** settings. Users can select any of the following two options from the **Vacation Encashment Calculation Method** dropdown field**.**

1.  **Based On Current Daily Rate**: current functionality/ default setting

2.  **Based On Vacation Accrual Average**: Daily rate = total accrual amount/ total accrual days

![](/img/product-enhancement/image50.png)

Another requirement of the client is to have an option to specify how arrears are recalculated against salary revisions. For this, a new dropdown field (**Accrual Amount Arrear Recalculation on Salary Revision**) is added to the **Vacation Scheme** menu from which any of the following options can be selected as required:

-   **For Full Accrual Days Balance:** current functionality/ Default setting

-   **From Salary Revision Effective Date**: recalculate accrual only from salary revision period

-   **No Calculation**: no recalculation of accruals on salary changes\
    \
    ![](/img/product-enhancement/image51.png)

### Leave split up based on leave balance

#### July 2018 (#10175)

Normally, if an employee is delayed from annual leave, system will be extending the leave with annual leave itself. But, if the employee doesn't have sufficient annual leave balance, then the system will split up the leave extension in to two- one with available annual leave balance and the rest with unpaid leave (salary will be zero and accruals are excluded for these days).

This functionality is achieved by introducing a new parameter **Default Leave Type in Leave Return Screen for Delayed Return if Balance Exceeds** in Leave Master for leaves having leave type category 'Adjust Vacation Balance' ![](/img/product-enhancement/image52.png)

While creating Leave Return, the user can create a new leave based on 'default leave type in leave return screen for delayed leave' if employee have enough leave balance. Otherwise, return leave will be split in to two leaves, first leave days will be leave balance days, second leave will be difference between total leave days and leave balance. User will not be able to modify this leave data.

### Time Lieu Expiration

#### July 2018 (#8245)

There was no expiry for employee's Time in Liew hours. On the **Application Parameter>LV-1 tab,** a new parameter (**Cut-off for availing Compensatory Off Balance)** is added to set a cut-off value so that the Time in Lieu hours will expire after a predefined value (Days).

![](/img/product-enhancement/image53.png)\
The values available for selection are listed below. These values refer to the time period for the expiration of the compensatory off. For instance, If the value is set as 30 days, the employee will have to avail this compensatory off within this 30 days' time, otherwise it will get expired and so it will not be available for him.

-   30 Days

-   60 Days

-   Till end of Next Month

-   Till end of next 2 months

-   Till end of Current Month 

**Max -ve allowed Compensatory-Off Balance** parameter in **Application Parameter>LV-1** tab is used to validate the leave days while taking the compensatory off Type Leave.

For instance, on assigning a value 5 for this parameter, while applying for compensatory off, even though the leave balance is 17, an additional 5 leaves also can be applied.![](/img/product-enhancement/image54.png)

### Probation notification with Attachment

#### July 2018 (#9955)

Here the customization requirement was such that an email notification should be send to the Line Manager and head of department 30 days before the probation end date with Probation evaluation form. The header details of Probation need to be filled based on employee's data. The probation notification to Managers should only be send once.

The solution approach is done in the following manner:

-   This feature is handled by introducing a new alert type in Alert Configuration screen - 'Probation Alert Prior to

-   Probation End Date'.

-   The custom report should be made for Probation Report and this will be available for selection in Alert

-   Configuration.

**Note:**

-   Alert functionality is moved from **HRW License Manager** to **Email Attendance Details** service.

-   A new key is added in HRW License Manager and Email Attendance Details -- **EnableAlert** which will be **true in former and false in later (by default**). This parameter, if set as true, Alert functionality will be working as usual. This key **should not be** set as true in both services.

-   The probation report uses **Report Emailer** application for mail sending, like the Attendance Report emailing functionality. So the URL to Report Emailer exe should be set in service's config file.

-   The key **TimeInterval** used in License Manager is also added in Email Attendance Details service (default as 1)

![](/img/product-enhancement/image55.PNG)

### Dashboard

#### July 2018 (#5266)

Development of Dashboard Module in HRW. This include the following sub modules:

-   **Widget Creation**: This screen is mainly used to create a dashboard widget of any type.

-   **Dashboard Designer**: Once the widget is created, the user can design the dashboard.

-   **Dashboard Viewer**: Finally, the dashboard can be viewed from Dashboard Viewer screen.

    -   The dashboard viewer working is based on the settings in widget creation and the design in the dashboard designer page.

    -   The filter option in Dashboard viewer is based on the filter option of each widgets in widget screen.

    -   The dashboard user can filter the dashboard from the filter option set in widget screen.

    -   If the filter selection is checked in 'Is user overridable', then the user can select all available filter options otherwise user can apply only the selected filter options in the widget screen.

## Common

### Employee Master Report

#### Jan 2021 -  #15338

The client required an option to print the master reports in HR Works.

In order to implement the feature, follow the steps below:

-   Execute two scripts to enable the custom reports menus.

-   Execute the custom SP.

![](/img/product-enhancement/image56.png)

### Letter and certificates Sequence format

#### Nov 2019 -  #17192

Generate Sequence number based on certain conditions such as include generation date, doctype, employee code along with sequence while showing in Personal Records

Execute the customized stored procedure for Sequence no Generation.

![](/img/product-enhancement/image57.png)

### Employee Detail report without Salary

#### July 2019 -  # 16094

In some companies, some of the members in the HR team do not have the access to salary of the employees. Hence, the client required to view the employee detail report without the salary information.

For implementing this feature, execute an SQL.

![](/img/product-enhancement/image58.png)

### Send ICT form on Employee creation

#### May 2019 -  # 14851

The client requires to send an ICT form to the IT department head while a new employee is created in the system. In this format we are not sending employee details.

For this, a new view is created. Execute the view.

In Card Types >> Other Cards, create the fields to Upload and attach the files in the Card Master and name the action form as NEWEMP.

Crete the workflow for sending the email and link it with the action form.

![](/img/product-enhancement/image59.png)

### Dashboard Items

#### March-2019 -  #13835

The client required to view the below details in the Dashboard window.

-   Pending Leave details upon which leave settlement is not processed.

-   Pending separated employee details, whose final settlement is not processed.

-   Upcoming Leaves of 30 days, where leave start date is within 30 days from today.

-   Alert Details of Company Document.

-   Alert details of Employee Document.

Changes based on the customised SP

![](/img/product-enhancement/image60.png)

*Fig: Dashboard Viewer*

### Transaction Period option (as same feature available in TAS)

#### March-2019 -  #14031

The transaction period of the reports is customised in this point. The requirement of the client was to view the transaction period in three different ways such as Today, Previous working Day and Period. However more options for selecting the transaction period has added in this point. This change is applicable in both HR Works and HR Work Plus in the general transaction screen and report screens.

The changes are made as follows.

1.  Three new parameters named 'Default Date Range in Transaction & Reports', 'Default From Date' and 'Default To Date' are added in the TAM-1 tab in the Application Parameter.

2.  Execute a script for enabling the newly added parameters and its selections.

![](/img/product-enhancement/image61.png)

*Fig: Application Parameter Settings*

### Company Wise Employee Strength Report

#### March-2019 -  #12954

The client requires to view the Company wise employee strength indicating the number of employees working, absent, on annual leave and E-Leave. Follow the below steps.

1.  Run the SP (In the SP you can change the leave which you need to filter.)

2.  In the Custom Report Designer, create the Template and set Report Source and set the parameters.

     ![](/img/product-enhancement/image62.png)
>
- *Figure: Report showing employee strength*

### Employee Head Count Report

#### March-2019 -  # 12953

The client required to view the number of employees working in each project separately.

Therefore, a report was developed showcasing the project name, project manager and number of employees by grouping the employees based on the destination.

1.  Execute the SP for getting the head count details.

2.  In the Administration Module, in the Custom Report Designer, create the Template and set Report Source as the Custom SP and set the Custom Report Module with required module.

3.  Set the required parameters of Custom SP and the data types in User Entry Values.

4.  To view the report, Select Document Template and set parameters for the template and Preview.

![](/img/product-enhancement/image63.png)

- *Figure: Report showing employee strength in each project*

### Additional Field in Employee Profile Report

#### March-2019 -  # 14269

In the Employee Profile Report, there was no option to add the photo of the employee. As per the client requirement Employee Profile Report was customised by adding the option to add the Photo of the employee, gratuity as on date and leave balance as on date.

In order to implement this, an XML file and REPX file must be executed.

The fields required in the Report Template screen is saved through the XML file.

The format for the report is designed in the REPX file.

![](/img/product-enhancement/image64.png)

*Figure: Employee Profile Report*

### Entity Headcount Report

#### Feb-2019 -  # 13464

The client maintains Designation, Department and Grade as the positional entities. There was no provision to get the head count of the employees by these entities in form of a report. Therefore, a new report named Entity Headcount report in which entity wise head count can be viewed is introduced.

Changes based on view and xml

![](/img/product-enhancement/image65.png)

*Figure: Custom Reports- Common*

### Dash Board to show Pending Approval Items

#### Feb 2019 -  # 12361

Initially, Pending Approvals were listed in the Pending Action Screen in the Administrative module on the basis of the access rights. Now, the client needed a dashboard that shows pending approval items with the user, when logged into the HRWorks. Need to show the dashboard as landing page. These changes are made as below:

1.  A new menu '**Dashboard Pending Actions**' is added to HRW

2.  Changes based on SP

![](/img/product-enhancement/image66.png)

*Fig: Dashboard viewer*

### Employee Report with specific format

#### Dec 2018 -  # 13628

The client needs to generate employee report in specific format in HR Works. A new field (Date of birth) has been added to personal record report.

![](/img/product-enhancement/image67.png)

### File upload restriction in document upload

#### Dec 2018 -  # 12051

The client wanted to set the maximum upload file size for the following:

1.  Employee photograph upload (in Employee Master data screen)

2.  Fields with its datatype set as file in Personal Records fields

For employee photograph upload, a new parameter named **Maximum Size of Employee Photo in KBs** is introduced on the **Application Parameter >> HRW-1** tab. Users can specify the maximum upload limit for employee photograph in kilobytes.

For the fields on the personal records field groups for which you want to have file upload size limit, do the following:

-   A query needs to be executed in the HR Works DB, in which the type and size of the file to be uploaded can be specified.

-   Once the query is executed, an appropriate value will be available for selection in the **Validations** dropdown field against any of the personal records fields provided it's data type is set as *File*.

### Employee status Oct 2018 (#11056)

Currently the status of employees such as terminated, separated, absconding, and on leave are system generated statuses meaning they are generated by the system based on the respective transaction. In order to track employee's current and last status, the client requires additional statuses such as Cancelled, Left country, Run away, and Police case to be set for their employees.

For this, a new field is provided in the Employee master for users to update the status with effective date. Status needs to be updated even if the employee is separated. The Employee list now shows employee's system generated status as well as the status that is set manually.

Based on XML change

### Service day in employee Master

**Oct 2018 (#11912)**

Employee master should show service days as of the current date in Year/s Month/s Day/s format. Date should be displayed on the employee's master screen below his/ her age.

A new parameter is added so that users can specify the format in which service days is displayed (**Application Parameter >> HRW-1 >> Service Days Display Format**).

![](/img/product-enhancement/image68.png)

If an employee is terminated or is included in employee separation transaction (Payroll Action form) and if the effective date on the separation record is less than the current date, then his/ her service period is calculated and displayed as follows:

No. of service days (Current Date - Hire Date) + 1) till the effective date .

### Select all option in the personal record change action form

#### July 2018 (#10647)

Implemented Select 'all' option for personal record action form on employee list grid. Maximum 5 Employees personal records can be updated through manual entry. The user can generate excel template for all employees and upload the personal record.

![](/img/product-enhancement/image69.png)

### Organization Chart Design

#### July 2018 (#7971)

-   Added new column named 'Organization Chart By' in Organization Chart Types screen.

-   Entities with 'This Employee' data type is shown in this column.

-   The value of this column is used for the new JSON Organization chart.

-   Setting the 'Organization Chart By' value will also enables you to filter the data that needs to be shown in Chart by different Entities within an Entity Type. (Image 1)

-   These selections are shown in Selected Values column.

-   We can show / hide the nodes by clicking on the green arrows in the organization chart. (Image2)

-   Entity Type with Sort order 1 is shown in the organization chart.

![](/img/product-enhancement/image70.png)

Image 1

![](/img/product-enhancement/image71.png)

Image 2

## Reports

### Option to show Salary history in Employee Profile Report

#### Sep 2019 -  #16374

A customisation was made to view the salary history in the Employee profile Report of the employee.

To implement this change, follow the steps below.

1.  Execute an sql

2.  Import a repx file to custom report.

![](/img/product-enhancement/image72.png)

### Missing Document Report

#### July 2018 (#10782)

New Report has to be generated for obtaining missing document of employee based on document type. We have added a new report Missing Personal Records. Here, on selecting corresponding document types missing cases will be displayed on previewing.

![](/img/product-enhancement/image73.png)

### Company name in reports with Export Friendly option

#### July 2018 (#10111)

Company name and report title are now included for the following screens where export friendly print option is used:

-   Attendance Integration

-   Cost Allocation Report

-   Payroll Sheet

-   Employee Details

-   Indemnity Accruals

-   Leave Report

-   Air Ticket Accrual

-   Period Summary Report

-   Salary Register

-   Salary Transfer Details Report

-   User Defined Accrual Register

-   Vacation Accruals

     ![](/img/product-enhancement/image74.png)

### Company document in expiry report

### July 2018 (#10141)

We have provided a new screen for company records and in this screen the Company Document Expiry details will be displayed and in Document Expiry report the Employee Document Expiry details will only be displayed. The setup and configuration are all the same as that of Employee document expiry screen and the data are displayed based on alert in each report screen.

The format of the report is embedded here:

![](/img/product-enhancement/image75.emf)

![](/img/product-enhancement/image76.png)

## Payroll Reports

### Time sheet validation for Approved leave days

#### April 2021 -  #21807

The client required the following validations in both HR Works and HR works plus Timesheet if approved leave application exist for the day.

-   If an employee have an approved leave application for a day then the Time sheet is not required for that day.

-   Exemption of Time sheet entry must only be applicable for applied/approved leave and must not be applicable for delayed leaves.

-   Leave days' Time sheet must not be considered in Missing Time sheet.

-   If employee cancels the Leave for the current month then Time sheet must be mandatory for the day.

-   Leave Days Project, Subproject and Team in TS must be taken from Employee master.

In order to implement the change, execute an SP.

![](/img/product-enhancement/image77.png)

![](/img/product-enhancement/image78.png)

### New report format to be added in salary register

#### Jan 2021 -  #16446

The client required some format changes in the new salary register. Moreover, in the present system, if a mid of month salary revision in a current month occurs, then it is showing as LOP in the system. Rather, than showing LOP, it must be shown as negative arrear.

In order to implement the change follow the steps below:

-   Execute the report template xml

-   Execute the query to enable the application parameter menu.

![](/img/product-enhancement/image79.png)

### Non payable allowance not to show in the Employee profile report

#### Nov 2019 -  #17433

The client does not require to show the non-payable allowance for the employee in the salary details while printing employee profile report from Common-\- Employee.

To implement the change, follow the steps below:

1.  Execute the Report Template script.

2.  Execute the xml

![](/img/product-enhancement/image80.png)

### Changes in bank Transfer file

#### Dec 2019 -  #17806

The client required the below changes in the bank transfer file.

-   To remove the bank code column

-   Change transaction type to 'SAL' for Payroll and Leave Settlement batch, 'EOS' for Final Settlement batch and 'ALW' for Off cycle batch.

Execute a view to implement the change.

![](/img/product-enhancement/image81.png)

### Salary Transfer Summary Report

#### Dec 2019 -  #17606

The client required to provide an option to take Summary report of Salary Transfer. Further needs an option to group with 'Payment type' and 'Bank' and must provide total as per grouped items.

To implement the changes, follow the steps below.

1.  Execute XML file.

2.  Execute 4 REPX file.

![](/img/product-enhancement/image82.png)

![](/img/product-enhancement/image83.png)

![](/img/product-enhancement/image84.png)

![](/img/product-enhancement/image85.png)

### JNJ Data Comparison Report for Temporary Earnings

#### Dec 2019 -  #16862

The client required a report that lists all the temporary/one time payments provided to employees in a payroll period.

In order to implement the change, execute the XML.

![](/img/product-enhancement/image86.png)

### Option to suppress values without account number

#### Sep 2019 -  #16608

A new feature to suppress the values without account number was introduced in the GL Account statement.

To implement the change, execute an xml file.

![](/img/product-enhancement/image87.png)

### Entity changes related corrections in payroll reports

#### Aug 2019 -  #15830

Currently the access rights for payroll and user has been given in the entity level - payroll location. Therefore, while transferring an employee from one payroll location to other, the existing payroll batch disappears and the users were not able to view the previous batches in the system. Hence, a new feature was added to view the previous payroll details of the employees even after changing the location.

For implementing this change, follow the steps below

1.  In the PRL-1 tab of Application Parameter set the entity values based on batches.

2.  Run an SP.

![](/img/product-enhancement/image88.png)

### Option to show paid annual leave salary in salary register

#### Aug 2019 -  #16001

Currently, in the salary register the salary paid during the annual leaves are not shown. Hence, the client required to view the salary paid during the annual leave in a separate column in the salary register and the corresponding deductions must be reflected in the remaining components.

For implementing this change, execute the xml.

![](/img/product-enhancement/image89.png)

### Employee Distribution Report

#### May 2019 -  # 6948

The client required a new report named Employee Distribution Report which is a two-dimensional entity report where the entities are taken from the positional and personal entities.

Execute a query for enabling the report.

![](/img/product-enhancement/image90.png)

*Fig: Employee Distribution Report*

### Account No and remarks in pay slip

#### Dec 2018 -  # 14045

Client requires that the following two more details be shown in their pay slip

1. Account no (entered in the employee \- Payment details \- Account No

2. Loan Remarks in the deduction part (remarks which is entered through loan application)

This is implemented by executing xml script in the db.

### Budgetary Report for Company Records

#### Dec 2018 -  # 12535

The client has a field for displaying amount, which they maintain as the expiry document budget amount, for all items on their company records menu. These data can be viewed as a report when they generate the **Company Document Expiry** report from the Payroll Management module >> Reports section. The client wants to have the total amount for each item on the Company records displayed in the **Company Document Expiry** report which shows the total of all the values in the respective **Amount** field.

![C:\Users\\ADARSH\~1.ADA\\AppData\\Local\\Temp\\SNAGHTML2e899d7f.PNG](/img/product-enhancement/image91.png)

### All company Consolidated report

#### Dec 2018 -  # 12677

The client who has multiple company set up in their DB wants the following:

Now, on the Common >> Company Records menu, users can view the company records of the company they have logged into. The client requires that the users should be able to view the company records data of all the companies in the database regardless of the company they have logged into. For this, user can now configure the Custom Reports in the following manner:

-   A few Stored procedures need to be executed in the HR Works DB. Separate SPs are created for every item on the Company Records menu. Only when you execute the SP meant for an item, the respective view will be available for selection in the **Report Source** dropdown field on the **Custom Report Designer** screen.

-   On the **Administration >> System Management >> Custom Report Designer** screen, a new report will be created by selecting appropriate values from the **Report Source** field. The values that you select here appear as result of executing the respective Stored Procedure in the HR Works DB.
    ![](/img/product-enhancement/image92.png)

-   From the Custom Reports Module dropdown field, select the module in which you want to have the **Custom Reports** menu appear.

     ![](/img/product-enhancement/image93.png)

    ![](/img/product-enhancement/image94.png)

### Absent details in pay slip

#### **Oct 2018 (#12250)**

For one of the clients, the interface period is set as *21st of previous month to 20th of current month* (see **Application Parameter >> TAM-1 >> Absent Leave Integration Period** parameter). The client requires that the reprocess records also be treated as absent. Changes have been implemented so that for whichever date the employee has reprocess (missing IN/ Out), those dates will be shown as absent in his/ her pays lip. The system will show the from date and to date of the days having reprocess as absence in employee's pay slip in its leave section.

### Salary Pay-out report

#### July 2018 (#10489)

Added new report "Salary Pay --out"

The report is included in existing Payment Summary Report. It is done as an XML change. Data will be shown in two grids which can be printed in Excel document.

![](/img/product-enhancement/image95.png)

![](/img/product-enhancement/image96.png)

### Custom Separated Employee Report

#### July 2018 (#10487)

New Custom report is added through Report Template Designer. Execute the xml in order to include this report.

![](/img/product-enhancement/image97.png)

### Header/Footer data with/without column headers - WPS

#### July 2018 (#10727)

Usually in case of reports like WPS, report format for Header-Footer were pre-defined and couldn't be changed. Now with this feature we can make changes using XML file.

### To implement Printable Version of Change Control Report

#### July 2018 (#8259)

Added new **Change control report** Format.

In this format Company, Business Unit, Division and Department details (show only code) will show after the employee name column.

-   In Earning Deduction Batch, add New Column  Type to show the Pay Component type Earning or Deduction. Rename the Existing  Type field as  Component 

-   In Overtime Entry, please show the amount also

-   Please show the destination in Air Ticket Availment

All these requirements have been implemented in the report. Changes are done using XML and customized SP.

![](/img/product-enhancement/image98.jpeg)

### Employee document for all companies in single report

#### July 2018 (#10143)

Now we have an option to print the employee documents and company documents for all companies from the login company. Two customised SPs are used to show all Company document and Employee documents through the Custom Report Designer. On selecting the required SPs from the combo selection and by enabling respective menu Reports can be viewed.

![](/img/product-enhancement/image99.png)

### Configurable separator for CSV and Text file formats

#### July 2018 (#10726)

Usually in case of reports like WPS, the report format along with the separators used for differentiating items where pre-defined and couldn't be changed. But now, by implementing this feature, the separators can be configured for both csv and text file. This is done by executing xml.

![](/img/product-enhancement/image100.png)

![](/img/product-enhancement/image101.png)

### Position Entity Codes to be added in column chooser

#### July 2018 (#11032)

The requirement is to add Position Entity Codes in column chooser for the following reports:

- Salary Audit Report -- Variance
>
- Overtime Report
>
- Loan deduction List Report
>
- Indemnity Accrual Reports
>
- Air Ticket Accrual Report
>
- Vacation Accrual Report
>
- UDA Accrual Report

The feature is implemented in all of the above reports except salary audit report.

### To show the contents of remarks field of an Earning Deduction Batch in the Payroll Comparison report

#### July 2018 (#10882)

Earlier in payroll comparison report comment field for earning deduction record it will display comment 'EarningDeduction.

Now it will show the actual remarks we have entered through Earning deduction screen.

## Time and Attendance Module

### Customisation in Employee Break Permission

#### Jan 2021 -  #20655

The client required some customisation in the Employee Break Permission screen which are explained below.

The client required to select the option for reason in the Employee Break permission screen through a dropdown. The options for reasons must be able to populate from the master screen of the application. Moreover, the user must be able to add, edit, search and set the sort order for the Reasons in Master.

Consider an employee working under 8:00 AM to 05:00 PM shift having Break permission from 10.00 AM to 12.00 PM.

-   In case the employee exits the site at 10.00 AM and if the report is generated at 11:00 AM, it should reflect that the employee is on break permission on the Status column.

-   If employees use break permission, the corresponding permission type should be reflected in the status. (E.g.: Official mission, personal sick, schedule off, etc.)

-   And when the employee reenters the site after permission at 12.00PM and the report status message shall change from break permission options to "Swiped In" or "Present".

-   Similarly the below scenario must also be considered.

-   If the employee has permission from 10 AM to 12 PM.
    • Employee left at 9:45 and came back at 12: OK\
    • 9:45 AM and 12:15 PM: Not OK\
    • 10:00 AM and 12:15 PM: OK

In order to implement the change, follow the steps below:

-   Execute the Report Template and Custom xml.

-   Execute query for enabling parameter 'EntityID (Reason Code) in Break Permission. Set Application parameters 'EntityID (Reason Code) in Break Permission'

![](/img/product-enhancement/image102.png)

### Automatic Email Notification from HRWorks

#### July 2021 -  #21149

The client required to send a notification to HR team if an employee is absent for 20 days (Not Continues) without any notification within a contract period. The automatic mail must be generated after 20^th^ absent day.

In order to implement the feature, follow the steps below.

1.  A new Email Setting for Previous Day Attendance Exception emailing must be created.

2.  Set Exception Exceeding Days as 19 (generates email when exception exceeds 19 days. ie. 20 days)

3.  Two parameters must be added in User Defined tab to identify From Date and To Date which should be provided for each employee.

4.  Execute the xml after providing the following values

-   EmailTypeId of newly created Email Setting from HRW_EmailTypes table

-   FORMAT Continuous - Exception email generated only if exceptions occur for given number of days consecutively in the given period.

-   Non Continuous - Exception email generated if exceptions occur for given number of days in the given period.

-   EXTENDED:
    * TRUE - To generate email when given number of exception days is exact or greater in the given period.
    * FALSE - To generate email when given number of exception days exactly matches the exception days in the given period.
    
    ![](/img/product-enhancement/image103.png)

### Option To Upload Weekly Shift Schedule

#### Feb 2020 -  #18113

The client required an option in the Weekly schedule setting to upload the shift schedule against an employee or an entity with an effective date.

![](/img/product-enhancement/image104.png)

### Option for uploading documents while entering overtime

#### Jan 2020 -  #17531

The client required to upload documents along with Overtime batch entry. Therefore, required an option to upload document while entering Overtime through overtime entry screen. Further the user required to view/ download each employee uploaded document via OT batch report screen.

To implement the changes, execute a query.

#### ![](/img/product-enhancement/image105.png)

### Converting holiday to absent day if the previous day is an absent day

#### Dec 2019 -  #17823

The client required to consider holiday as absent, when the employee is absent on the previous day of a holiday.

![](/img/product-enhancement/image106.png)

### Changes in Time sheet format for EIFM

#### Nov 2019 -  #17097

The client required to revise the timesheet format for EIFM. In the earlier period, the project was taken from the sheet name inserted in the excel file. Now, in the proposed change, the project is added as an additional column in timesheet. Employee working in different project will have multiple lines and data in the days column will be validated as per the project transfer details similar to old format.

To implement the change new column for project was introduced.

![](/img/product-enhancement/image107.png)

### Shortage on Reprocess Day

#### Oct 2019 -  #16377

The client required to display the shortage in the reports, if an employee has an attendance exception i.e. missing In or Out.

 Consider Reprocess in Attendance Calculation As . Options shall be Absent/Shortage. If this Parameter is set as Shortage, hide Calculate Total Hours option in policy code and total hours shall not be calculated in this scenario.

![](/img/product-enhancement/image108.png)

### Attendance Related Violation Report-Employee Wise

#### Oct 2019 -  #16368

The client required a new attendance report which lists the Employee wise attendance violation and Overtime.

To implement the change, execute a Report Template script.

![](/img/product-enhancement/image109.png)

### Hourly Rate allocation report

#### Oct 2019-# 16250

The client required hourly rate allocation report to be generated from the system for the selected period batch. The report will be generated Cost centre wise, hourly rate category wise and project wise. The report must consist of the following fields

-   Cost Centre (Position Entity)

-   Hourly rate Category (Position Entity)

-   Account no (Cost centre and hourly rate will have an account no.)

-   Project (From Time sheet)

-   Sum of Hours (Total hours worked by employees in the specific Cost centre and hourly rate category for the project. This total hour includes the OT hours.)

-   Rate (Rate shall be defined for each Cost Centre and hourly rate category. This rate shall be shown here.)

-   Sum of Amount (Sum of HoursRate)

To implement the change, follow the steps below:

1.  Execute an XML

2.  Execute a query

3.  Execute a report template query

4.  Set multiple application Parameter for 'Timesheet Rate' in RPT-1 Tab based on cost centre and Hourly Rate

### Department Head Attendance Detail Report

#### Oct 2019 -  #16471

The client required a new attendance report which lists the Department Heads day-wise Attendance data.

To implement the change, follow the steps below.

1.  Execute a report template XML.

2.  Execute an XML for department head wise filtering.

3.  Execute a script for active employee filter.

![](/img/product-enhancement/image110.png)

### Department Head Attendance Summary Report

#### Oct 2019 -  #16470

The client required a new attendance report which lists the Department Heads Attendance Summary.

To implement the change, follow the steps below.

1.  Execute a report template XML.

2.  Execute an XML for department head wise filtering.

3.  Execute a script for active employee filter.

![](/img/product-enhancement/image111.png)

### Display Break Permission Remarks in Attendance Report

#### Sep 2019 -  #16437

The client required a new column in the Consolidated Attendance Report to view the remarks updated during the break permission submission. The user enters the break permissions data from Employee Break Permission screen in HRWorks.

Execute an XML file of report template to implement the changes.

![](/img/product-enhancement/image112.png)

### Customization in Weekly Schedule setting screen

#### Aug 2019 -  #15190

The client required the following changes in the employee wise weekly schedule settings screen,

1.  Introduce a new column in the main grid for displaying IQama ID from the Personal Records.

2.  Selected entities using the  show filter should show in the main grid

For implementing this change,

1.  Execute the query to enable the show filter

2.  Execute the view.

3.  Execute the xml to show to show the entityid and the Header description of Grid column.

![](/img/product-enhancement/image113.png)

### Adjust Shortage hours with Over Time during Payroll calculation

#### July 2019 -  # 15108

A new functionality for adjusting the shortage hours with the overtime is introduced in HR Works. Shortage hours is adjusted from overtime hours in an order of Holiday OT, Off day OT and Normal OT during payroll calculation and the remaining OT hours only will be paid. Further, this functionality must be linked on Department level.

Changes based on customized SP.

![](/img/product-enhancement/image114.png)

### New Report - Attendance Summary Report in HR Works

#### July 2019 -  # 15424

The client required to create the Attendance Summary Report in a new format.

For implementing the changes on the Attendance Summary Report, follow the steps below:

1.  Execute a query for enabling the menu.

2.  Import REPX in the Custom Report Designer.

![](/img/product-enhancement/image115.png)
Figure: Attendance Summary Report*

### Absent Report Emailing

#### May 2019 -  # 14615

The client required to send the Absent Summary Report via email to the supervisors and HR.

For this, Run the script for custom report designer.

Run the script for email settings.

Run the script for identifying the Entity Types.

Import the REPX file for the report.

![](/img/product-enhancement/image116.png)

*Figure: Absent Report Generated*

### List Refreshment Allowance in Consolidated attendance Report

#### March-2019 -  #13346

The client required to view the details of the refreshment allowance of the employees in the Consolidated Attendance report which was not included in the report.

- Created new XML'ConsolidatedRPTTemplate' Report template for showing ' Refreshment allowance'.

     ![](/img/product-enhancement/image117.png)

    Consolidated Attendance Report showing Refreshment allowance*

### Project Details in Consolidated Attendance Report

#### Feb 2019-  # 12955

The Project Details of the employees were not recorded in the Consolidated Attendance Report. Now the client requires the project information to be listed in Consolidated Attendance report. They also need the provision to list multiple project information, in case the employee has worked on one or more projects for a day. For this, an XML script is executed in the DB. Given below is a screenshot of the **Consolidated Attendance Report** once the changes were implemented.

![](/img/product-enhancement/image118.png)

### Handling OFF day following an absent day

#### Feb 2019 -  # 13434

Initially, while calculating the attendance of an employee, all the off days were considered as the OFF day.

The client needed to calculate the attendance in the following way:

1.  If an employee is absent on the day just before an OFF day that OFF day should to be considered as Absent.

2.  If there are consecutive off days after the Absent day, all the OFF days should be considered as absent day.

These changes are made as below.

1.  In the **Policy Code** Screen, two new fields were added named as 'Weekly Off Following Absent Days' & 'Public Holiday Following Absent Days' in the in the Weekly Off /Public Holiday Parameters section.

2.  New field display will be based on the query

![](/img/product-enhancement/image119.png)

*Fig: Policy Code Screen*

### Yearly Weekly OFF Limit

#### Feb-2019 -  # 14036

Initially, while calculating the attendance of the employees, all the Weekly Off days where considered as Weekly Off irrespective of any limits.

The client needed to set the Weekly Off days limit for a calendar year to all the employees. After the Weekly Off Day limit, the leaves must be considered as absent.

These changes are made as below.

1.  In the **Personal Records**, add a new field named 'Weekly Off Limit' which contains the Effective Date, Limit and Remarks.

2.  A query is executed in the DB for activating the functionality

![](/img/product-enhancement/image120.png)

*Figure: Weekly Off Limit*

## TAM Reports

### OT Upload Report

#### Jan 2021 -  #19523

The client required an OT upload report in excel format that displays Total OT hours, Break up of total OT hours (Normal OT, weekend OT, and Holiday OT) and Approved OT hours. Further, the option to select From and To date must also be included. This report is to be uploaded in the customer's ERP system, so each column should be separate - no merged columns are allowed.

In order to implement the change

-   Execute the xml.

-   set ExportFriendlyPrinting option as true in Appconfig.

![](/img/product-enhancement/image121.png)

### Additional Column in Period Summary Report

#### Feb 2020 -  #18554

The client required to add a new column in the period summary report named period shortage which displays the difference between the required hours and total worked hours.

![](/img/product-enhancement/image122.png)

### Shift Details Report

#### Jan 2020 -  #17203

The client needs to develop a new report in HR Worksto show employees' shift details.

To implement the report, follow the steps below.

1.  Execute a view file.

2.  Execute an SP.

3.  Execute queries to enable the Custom Report Designer and Custom Report Menu.

![](/img/product-enhancement/image123.png)

### Missing Time sheet report

#### Dec 2019 -  #17786

The client required to add the following in the Missing Time sheet report

-   Add employee name in Missing TS summary report Employee wise.

-   Add position entity details in Missing Time sheet report summary (employee wise) and detail report.

     ![](/img/product-enhancement/image124.png)

### Displayed Break Duration in Attendance Report

#### Dec 2019 -  #17818

The client required to display the break duration defined in the shift codes in Consolidated Attendance Report.

In order to implement the change, execute the report template XML.

![](/img/product-enhancement/image125.png)

### Changes in the Attendance detail report

#### Nov 2019 -  #17623

The client required a selection option in the report screen for printing the report with break or without break. Earlier, it was possible to print the reports with break only.

In order to implement the change, execute new custom report.

![](/img/product-enhancement/image126.png)

### Early & Late Arrival Reports with filter option in HR Worksand HR Works Plus

#### Nov 2019 -  #16720

The client requires EARLY & Late Arrival reports with the option to filter by hours such as more than 10 minutes late, etc.

To implement the changes, follow the steps below.

1.  Execute an xml to enable the functionality.

2.  Execute the report template XML

3.  Execute two scripts for enabling the filter option.

![](/img/product-enhancement/image127.png)

### Time sheet report with rate

#### Nov 2019 -  #16252

The client requires to generate Time sheet report for a month based on the uploaded data.
The report will be generated Project wise and hourly rate category wise. The rate will be calculated based on the Cost centre and hourly rate category of employee.

For labour supply employee the same format will be generated where the rate will be calculated based on labour supplier and category of the employee. The report will be generated Labour supply wise.

Execute an REPX to implement the changes.

![HR Works - UAE](/img/product-enhancement/image128.tmp)

### Changes in Hourly rate allocation report

#### Dec 2019 -  #17631

The client required the below changes in the

-   Amount to be shown with coma separator.

-   Amount, rate and Hours to be formatted to number when exporting to Excel.

-   Add Report heading as  Hourly Rate Salary Allocation- MMMMM YYYY .

-   Hours and Amount sum total to be shown for each Cost centre.

-   Hours and Amount sum total to be shown for each Account.

To implement the changes, follow the steps below.

1.  Execute a report template query

2.  Execute a xml

3.  Execute two query for Hys_appParamData

![](/img/product-enhancement/image129.png)

### Changes in Time sheet report without rate

#### Dec 2019 -  #17630

The client required to make some changes on the time sheet report without rate which are mentioned below.

-   Add subtotal for each table.

-   Reduce the font size of tables to '8'.

-   Off days and Public holidays must be marked in Grey shade.

-   Add total no of employee in each category i.e. against the head of STAFF shown the no of employees in staff with label  No of employee= 

-   If employee does not have any data for a particular cost center, then employee need not to be shown in the project

Execute a script to implement the changes.

![](/img/product-enhancement/image130.png)

### Changes in Time sheet Report with rate

#### Dec 2019 -  #17629

The client required to make some changes on the time sheet report with rate which are mentioned below.

-   Add subtotal for each table.

-   Reduce the font size of tables to '8'.

-   Off days and Public holidays must be marked in Grey shade.

-   Add rate column before Cost column which is used for cost calculation.

-   If employee does not have any data for a particular project, then employee need not to be shown in the project

Execute a script to implement the changes.

![](/img/product-enhancement/image131.png)

### Change in Excess Hours Calculation For Strict Shift Timing

#### Dec 2019 -  #17526

The client required to change the Excess hours' calculation similar to OT computation for policies having strict shift timing. However, the Minimum required OT parameter for computing the hour was not considered.

To implement the changes, follow the steps below.

1.  Execute the custom SP to calculate Excess hours similar to OT computation for policies having strict shift timing.

2.  Execute the changed report template to display the excess hours.

![](/img/product-enhancement/image132.png)

### Audit Report for OT Pre Approval

#### Dec 2019 -  #16784

The client required an audit report for OT Pre Approval to identify which user had uploaded the data into the application.

Execute a query to enable the report and thus to implement the change.

![](/img/product-enhancement/image133.png)

### Attendance Related Violation Report-Department Wise

#### Nov 2019 -  #16367

The client requires new attendance report which lists the department wise attendance violation and overtime.

To implement the change, follow the steps below:

1.  Execute the Report Template script.

2.  Execute the script to identify Department in Consolidated Attendance Report

![](/img/product-enhancement/image134.png)

### Time sheet report without rate

#### Nov 2019 -  #16253

Client require to generate Time sheet report for a selected month on the basis of Cost centre wise, employee category wise (Staff/labour) and Labour supply wise. In this report rate and cost details must not be included. If employee have worked on multiple project then employee must be listed in multiple lines.

To implement the change, follow the steps below:

1.  Execute the Report Template script.

2.  Execute the XML to show multiple project for single day.

![](/img/product-enhancement/image135.png)

### Email Template for Attendance Report-OT Hours

#### Oct 2019 -  #16469

The client wants to send a particular format of report as an email attachment. The attachment must be sent to the managers/HR with the data of their respective subordinates.

To implement the change, follow the steps below:

1.  Execute a REPX for Attendance Violation Report - OT Hours

2.  Execute a script for creating/importing Custom Report in Custom Report Designer

![](/img/product-enhancement/image136.png)

### Email Template For Attendance Violation Report-Late Arrival

#### Oct 2019 -  #16468

The client wants to send a particular report format as the email attachment. The attachment must be sent to the managers/HR with the data of their respective subordinates.

To implement the change, follow the steps below:

1.  Execute a REPX

2.  Execute an XML for email settings

3.  Execute another XML

![](/img/product-enhancement/image137.png)

### Break Permission Duration and Machine ID in Consolidated Attendance Report

#### Sep 2019 -  #16366

The client required a new column in the Consolidated Attendance Report to list the break duration of the employee. Moreover, the Machine ID of the Time In and Out Transaction is also displayed in the reports.

Execute a xml file to implement the change.

![](/img/product-enhancement/image138.png)

### Non-Attendance Booking Report with Total Hours

#### Aug 2019 -  #15488

The client required a new column in the non-attendance booking report displaying the total hours. This can be used for tracking the hours worked by the employees.

For implementing this change,

1.  Execute a xml to add report template for non-attendance booking report.

2.  Execute xml to set category for the report.

![](/img/product-enhancement/image139.png)

### Upload Option in Daily Time sheet

#### Aug 2019 -  #16194

The client required an option to upload the time sheet entities such as Project, Sub project and Team through Excel file.

For this, the time sheet entities must be linked as Time Sheet entity in TS-1 tab of the Application Parameter.

![](/img/product-enhancement/image140.png)

### Day Wise OT Payable Report

#### July 2019 -  # 15038

The client required a report that list the OT payable amount for each day. For this, a new grid was introduced in the OT Calculation Report Screen.

For implementing the change, execute a XML for enabling the Day Wise OT Payable Report.

![](/img/product-enhancement/image141.png)

### Total OT Payable Report

#### July 2019 -  # 15039

The client required a report that list the total payable OT amount. Hence, in the Period Summary Report new columns named OT Amount, Total OT Amount and Basic salary was added.

For implementing the change, follow the steps below:

Execute the Report Template to enable the Total OT Payable report.

![](/img/product-enhancement/image142.png)

### List Terminal Information in Attendance Reports

#### March-2019 -  # 14431

The client needed to list the terminal Information in the Attendance Reports.

In order to implement this, the following steps were followed.

For displaying the terminal information in the Consolidated Attendance Report, four additional field were added named as MachineID_IN, MachineID_OU, MachineID_IN_2, MachineID_OU_2 in the xml template. The header named could be changed according to the need of the clients. Moreover, the IN terminals and OUT Terminal details were introduced in the report.

Created new XML'ConsolidatedRPTTemplate' Report template for showing 'terminal information'.

![](/img/product-enhancement/image143.png)

*Fig: Consolidated Attendance Report with Terminal Information*

### List OT Payable Hours in Attendance Reports

#### March 2019 -  # 14430

The attendance report does not list the details of the OT payable hours. The client required to display the details of the OT Payable hours in the Attendance Reports.

The changes are made as follows.

Updated XML 'ConsolidatedRPTTemplate' Report template for showing ' OT Payable hours'

![](/img/product-enhancement/image144.png)

*Fig: Consolidated Attendance Report showing OT details*

### Break Permission Report

#### Dec 2018 -  # 12814

As per the client's requirement, a new report (Break Permission Report) is added to the **Reports** section of the Time and Attendance Management module. In this report, users can view the following:

-   break details which are entered through the Employee Break permissions transaction in HR Works

-   break permissions requests made through View Calendar transaction in HRW Web.

The following steps need to be followed to implement this report.

A query is provided, which when executed in the HRW database, will enable the creation of a break permission report template on the **Report Template Design** screen. Once you create the Break Permission Report, it will appear in the **Reports** section of **Time and Attendance** module.

![](/img/product-enhancement/image145.png)

### Periodic Summary Report for Emailing

#### Dec 2018 -  # 12896

The client wants the same periodic summary report in excel format in HR Works to be sent over email as an alert which will be generated monthly from the system.

Changes done by executing scripts (xml).

![](/img/product-enhancement/image146.png)

### Email Template for Attendance Exception

#### Dec 2018 -  # 13178

Client requires that all exceptions for the previous week to be send as attendance exception email alert. Changes based on XML and repx file need to be imported.

![](/img/product-enhancement/image147.png)

### New Time Card report for Jordan Company

#### Nov 2018 -  # 12886

The Time Card report in HR Works web used to show employee name and code together in row and beneath that the time card details of the employees as shown below.

![](/img/product-enhancement/image148.png)

It also did not show Branch, Location, and Department, which are positional entities in the client's DB. As per the client's requirement, changes have been made so that the report now displays employee code and name in separate columns. The report also displays the Branch, Location, and Department of employees in the respective column. The image below represents the Time card report as it is displayed after the changes:

![](/img/product-enhancement/image149.png)

The aforementioned changes to the report was made through the report template in HR Works.

### Break Details in Consolidated Attendance Report

#### Nov 2018 -  # 12562

The Consolidated attendance report in HR Works did not show the Break details such as (Break IN Time, Break OUT Time, and Break hours. As per the client's requirement the report now displays these details. If the employee has multiple breaks for a given day, the report will show those details as well.

![](/img/product-enhancement/image150.png)

The aforementioned changes to the report was made through the report template in HR Works.

### New format for consolidated attendance Report emailing

#### July 2018 (#9514)

New Consolidated Attendance Report for Email reporting which includes split shift.

![](/img/product-enhancement/image151.png)

### New Timesheet Report Format

#### August 2018 (#10740)

Added new timesheet report as shown below,

![](/img/product-enhancement/image152.png)

-   The report will be generated in excel format in the system as per the required format.

-   Shift - this row will show the employees assigned shift codes and Off Day code "O" for the date, if any leaves are present then leave code will be shown as well.

-   Working Hours- this row will show the regular shift hours for the specific shift.

-   Late Hours -this row will show the late arrival or early departure hours Late arrival + Early departure (i.e. Total deduction) for the date.

-   If the employee has any approved break permission for the date, then that approved hours is not considered as late IN or early OUT.

-   OTH - this row will show the pre- approved overtime hours matched with extra hours worked in the TAS for the date. If there is no pre- approval for the day, employee have overtime, but it is not shown here.

-   Total (OT) Single- this column will show the total normal day OT hours for the month.

-   Total (OT) Double- this column will show the total of weekend day OT hours & Holiday OT hours for the month.

-   Total (Late Hours) -- this column for total late hours. (Late coming + early out).

-   There are no headers like month, department, section etc. Month and year are shown as per the format. E.g.: Jan-18

-   Excel file name will be defined as Month and Year (MMYYYY).

-   Contains only one common header for all employees like Emp. No, Name, Type, Period, Total (OT) Single, Total (OT) Double, Total (Late Hours) etc.

-   The requirements are incorporated as XML change.

### Report for meal eligibility

#### July 2018 (#9388)

New report for the eligible meals for the employees. Currently we are defining the meal eligibility and visitor meal eligibility in personal records screen of each employee. This eligibility needs to be shown in the report for each day.

The report is implemented in the following manner:

-   Created New Report (Meal Eligibility Report) in TAM Reports module.

![](/img/product-enhancement/image153.png)

-   To select Date range, it contains the **from date** button and the **To Date** button.

-   Order of Grid column headers (**Break Fast, Lunch, and Dinner**) based on order from the Card Master screen (**Other Cards -\- Meal Types**).

![](/img/product-enhancement/image154.png)

**Case 1**: If Employee (Employee code = 13404) has Meal Eligibility -- **Meal combinations (BF -- LN - DN)**, and for the same day he has Visitor Meal Eligibility **(Meal type -- Break Fast, Number of Visitor Meal =4)** then, Report shows **Breakfast -4, Lunch -1, Dinner -1**

-   Visitor Meal Eligibility is displayed only when **Visitor Card** is set as **Yes**.

## Services

### General Email Service

#### July 2018 (#7694)

General Email Service using **DLIHRMailService** is implemented for the following email modules:

-   Email Settings in HR Works

    -   Attendance Exception

    -   Attendance Report

    -   Employee Self Service

    -   Previous Day Attendance Summary

-   View Calendar Requests and Approval in HR Works Web

-   Payslip emailing in HR Works

-   Transaction Email Alert in HR Works

-   Document Alert in HR Works License Manager and Email Attendance Details services (supports multi DB configurations in Email Attendance Details)

-   OT Pre-Approval Emailing in HR Works Web

-   Email Functionalities in HR Works Plus

### License Manager

#### July 2018 (#126)

It is the License Manager utility which determines the Access Control, Menu Visibility and other core constituents of the system. These settings are configured in the utility file to suit to various customer needs. In addition, the number of entities that can be created in the Card Type screen, number of users that can access the system etc. can be restricted by assigning proper utility settings.

## HR Works Plus

## Administration

### Company Documents in the web application

#### April 2021 -  #21440

A new screen "Company Documents" was developed in web application.

In order to implement the new feature, execute an SQL to enable the menu.

![](/img/product-enhancement/image155.png)

### Development of card types screen in web

#### April 2021 -  #21840

A new screen "Card Types" was developed in web application.

In order to implement the new screen, execute an SQL.

![](/img/product-enhancement/image156.png)

### Option to delegate the proxy role

#### Aug 2019 -  #16034

Currently it was not able to assign role to a proxy. Hence the client required to delegate the role to the proxy user during role delegation along with the approval delegation.

For implementing this change, a new parameter named 'Role Delegation Applicability' was introduced in the HSS-1 tab of HRW + Application Parameters.

Execute a query to enable the application parameter.

![](/img/product-enhancement/image157.png)

### List Employee Names in Alphabetical Order

#### Aug 2019-# 16305

The client required to list he employee name in alphabetic order in the Select Employee dropdown available for the Proxy users in the Attendance Regularisation screen.

For implementing this, a new parameter was added in the PROXY-1 tab of HRW + Application Parameters named  Employee List Order in Proxy List .

Execute a query to enable the application parameter.

![](/img/product-enhancement/image158.png)

### User Interface Changes to HR Works Plus

#### Dec 2018 # 12841

Added new parameter in HSS**-1** tab of the **HRW+ Application Parameters** screen.

1.  **Default Skin**: Changing the skin will change the appearance, theme palette and other user interface elements. Users can change the default skin set for ESS and/ or set any of the available skins by choosing the respective values from this dropdown. If no values are set for this parameter or None is selected as its value, the ESS will appear in the default skin.

**Note**: This can be done by any users having admin rights or having the required permissions to make these changes in HR Works. If you want to allow the employees/ ESS users also to change the theme set here or the default theme, see the details provided on the **Allow Skin Change By Users** parameter.
![](/img/product-enhancement/image159.png)

1.  **Allow Skin Change By Users**: We can set either Yes or No as the values for this parameter, which will decide whether to allow employees/ ESS users to change the skin of ESS application.
    ![](/img/product-enhancement/image160.png)

2.  **Allow Banner Change By Users**: By selecting Yes or No as the value for this parameter, you can allow or disallow ESS users to change the ESS banner.
    ![](/img/product-enhancement/image161.png)

### Change in Validation Message view

#### Dec 2018 # 12963

The validation messages that appear on transaction entry or processing are usually positioned at the bottom of the page/ form. Client wants it to customize it so that they can specify the position of all validation messages and also set the format like font size.

The following are done for implementing these changes:

-   Added new parameter in **HSS-1** tab of the **HRW+ Application Parameters** screen.

    1.  The **Form Validation Message** parameter has the following options as its values which the user can select and specify the position of the validation message:

        -   In Bottom

        -   In Center.

        -   If nothing is set for this parameter or None is selected, the message appears at the bottom-right of the page/ form.

            ![](/img/product-enhancement/image162.png)

1.  **Form Validation Message Font Size**: Users can specify the font size of the message text as follows:

    -   Small

    -   Medium

    -   Large:

    -   If no value is selected or None is selected for this parameter, the message text appears in the default font size.
        
        ![](/img/product-enhancement/image163.png)

## Employee Profile

### Gross Salary in Salary details

#### July 2018 (#10911)

Gross salary was not shown Payroll details of Employee profile in ESS. Now this has been implemented.

![](/img/product-enhancement/image164.png)

## Payroll Transactions

### Employee Transfer request screen

#### Jan 2020 -  #15180

The client required an option to transfer Employee Location and Department in the HR Works Plus with following facilities.

-   Employees and Proxy users should be able to process position change requests.

-   Employees must only be able to make request for them self. But the proxy user can make request for other employee (Possible to select only one employee at a time).

-   Current position details should be shown in the position change screen, but it should be non-editable.

-   Work flow structure must slightly differ. i.e. the managers in the proposed entities should be approver. Approval work flow structure is shown below.
    Group Head-\>Current Department Head-\>Current Function Head-\>proposed department Head-\- Proposed Function head>HR.

-   System should provide a report in request and all approval levels. This report should be available in Arabic interface also.

In order to implement the change, follow the steps below.

1.  Execute two queries to enable the workflow settings.

![](/img/product-enhancement/image165.png)

### Action form for payment details in web application

#### July 2021 -  #21983

Developed Action form for payment details in the web application

To implement the feature, execute a script.

![](/img/product-enhancement/image166.png)

### Loan Amendment in web

#### July 2021 -  #19610

Developed new screen for Loan Amendment functionality in web application

To implement the feature, execute a query which enables the menu.

![](/img/product-enhancement/image167.png)

### Employee Transfer Request

#### Dec 2019 -  #14738

Currently, in employee transfer requests, department manager requests for employee transfer by Location or Department.

Now the client required the following changes:

-   Add selection options "Permanent Transfer" and Temporary Transfer". If temporary transfer, there should be an option available to enter From and To date.

-   It should have Site (Current Site + New Site) + Unit (Current Unit + New Unit) + Department (Current Department + New Department) options to select.

To implement the changes, follow the steps below.

1.  Execute a script to make the value type Active, that enables this feature.

2.  Execute a query to Enable  Positional Entity Transfer Type in HSS-1 tab.

![](/img/product-enhancement/image168.png)

![](/img/product-enhancement/image169.png)

## HR Portal

### Bank Transfer File in HRW Plus

#### Jan 2021 -  #6505

A new screen 'Bank Transfer File' was developed in web application.

For implementing the new report, execute an sql for inserting and enabling the menu.

![](/img/product-enhancement/image170.png)

### Development of Employee master in Web Applications

#### July 2021 -  #21938

Developed new screen for Employee master in Web Applications

![](/img/product-enhancement/image171.png)

### Separated Employee Report in HRW+

#### May 2019 -  # 13927

A new report for showcasing the details of the separated employees are created in ESS.

Execute a query for enabling the report.

![](/img/product-enhancement/image172.png)

*Fig: Separated Employee Report*

### Export option in pay-slip

#### Jan 2019## 13470

Users were not able to export pay slips from ESS. As per client's requirement export option has been brought in for pay slips in ESS. Now the users can export the pay slip into PDF and/ or excel file formats.

### Reports to be added in ESS

#### July 2018 (#7951)

Added the following reports into ESS.

-   Employee Master Report

-   Document Expiry

-   Salary Audit

-   Salary Register

-   Salary Increment

-   Loan Enquiry Report

-   Final Settlement

-   Leave Report

-   Leave Balance Report

-   Salary Transfer Details Report

### Need more fields in  Field Chooser of HR works Plus

#### July 2018 (#9400)

Earlier Time IN and Time OUT fields were not shown in the Field chooser, now this functionality is implemented in 'Time Card Report' Screen.

![](/img/product-enhancement/image173.png)

### Option to show pay slip in HRW+

#### July 2018 (#11057)

Earlier only approved batches were able to view or generate payslip. Now even unapproved batches can be viewed in ESS with the help of a new parameter in HRW+ Application Parameters **'Show Payslip in HR Works Plus'** (HSS-1) with options, **None/Always/Only for Approved Batches/Only after Payment Date**. Display of Payslip is controlled based on this Parameter, However, default option will be 'Only for Approved Batches'. This parameter will be hidden by default.

![](/img/product-enhancement/image174.png)

### Absent Report

#### July 2018 (#9272)

A new report called as Absent report is integrated in the TAM Reports category of ESS.

![](/img/product-enhancement/image175.png)

![](/img/product-enhancement/image176.png)

### Need Day name column in HR works plus Consolidate report

#### July 2018 (#10305)

The Day Name column can be introduced into the consolidated report by using template in the following manner.
``` xml
<FIELD NAME= DayName SHOWDEFAULT=  />
```

### Logo option for TAM reports

#### July 2018 (#9406)

Logo option is implemented in all HR Worksand HR Works Plus reports.

## Self Service

### Changes required in the WTB Time Sheet Report of HR Works Plus

#### Jan 2021 -  #20689

Currently, the WTB time sheet report can be generated as per the Payroll Period. Now, the client required to generate the WTB timesheet report according to the timesheet interface period.

In order to implement the change, execute two queries to view multiple projects and to sort order the uploading timesheet.

![](/img/product-enhancement/image177.png)

### Allowance Request

#### Jan 2021 -  #14743

In the Allowance request screen, the client required some additional features which are mentioned below:

The client required to display different allowance types such as Car allowance, Education Allowance, Mobile allowance and other allowance.

**Education allowance** is eligible only for the manager and department heads.

In **Education allowance**, an option to mention student name, Age of Student, Year, School name and fees is required.

Provisions to attach two documents which are admission copy and fee receipt are required in **Educational allowance**.

Maximum two kids must only be eligible for **Educational Allowance**.

When the employee is terminated or resign from the job, the educational allowance will be deducted from the final settlement amount.

The employee could request for the educational allowance only after completing the probation period.

Education allowance could be requested once in six months only.

In order to implement the changes, following steps were executed:

-   Create a user defined parameter and corresponding fields in User Defined tab of Card Types.

-   Set value of parameter "Entity ID(UDA Details)" in UDA-1 tab of Application parameter to the created user defined parameter.

![](/img/product-enhancement/image178.png)

### Option to show Disclaimer in Leave Application Screen

#### Feb 2020 -  #18417

The client needs to show the company Leave Application policies in leave application screen.

Execute an xml file to execute the change.

![](/img/product-enhancement/image179.png)

### Business Travel Expense Claim

#### Jan 2020 -  #14728

The client required the provision for employees to request for business expense claim separately from the Business Travel Expense Claim screen after the business trip. Hence, there should be a link between business travel plan and business travel expense claim requests. The employee must be able to request for business travel expense claim based on the approved business travel plan and must be able to edit the travel information with actual data from the expense claim screen if required.

In order to implement the change, execute a query to enable the parameters.

![](/img/product-enhancement/image180.png)

### Business Travel Plan

#### Jan 2020 -  #14727

The client required an option to request for business travel plan before the employee goes for Business Trip and get it approved for travel.

In order to implement the change, the execute the query to provide a message to all approval levels.

![](/img/product-enhancement/image181.png)

### Loan Application Request/Approval Changes

#### Jan 2020 -  #14740

The client required some features in the Loan Application Request/Approval screen which are pinpointed below.

-   Loan request /Approval screens must display basic salary, gratuity amount, Hire date and last year performance.

- (Last year performance is a score entered in employee's personal records.
>
- The gratuity amount will be actual payable as per Resignation policy)

-   For all outstanding loans, the system should pop up/show a message in both request and approval screens.

-   Loan repayment amount should not exceed more than MOL maximum Salary deduction. If exceeded it should show as a message in the screen.

-   HRA advance request also needs to apply through loan application.

-   Number of instalments for HRA advance should be either 6 or 12. It can be linked to employee via parameter. By default, the option should be 6.

-   HRA advance request must be possible only after 5 months of loan repayment.

-   Option must be provided for approvers to edit loan amount and repayment schedule.

In order to implement the change, follow the steps below.

1.  Execute a query to enable the application parameter 'Show loan details'.

2.  Execute an SP for enabling the request HRA advance through loan application.

![](/img/product-enhancement/image182.png)

### Changes to be made in General Expense screen

#### Dec 2019 -  #15767

Initially in general expense screen there were different buttons for each tab. Now, as per the requirement, the single button is redesigned to multi button.

![](/img/product-enhancement/image183.png)

### Option for attachment for Approver in L&C

#### Dec 2019 -  #17603

The client required an option to attach a file while approving the request in L&C module.

To implement the change, execute a query to enable the application parameter settings.

![](/img/product-enhancement/image184.png)

### Loan Limit and Instalment limit Validation

#### November 2019  #17191

The client needs the following policy to be applied in Loan application. This change is particular to HRA Advance.

1.  Employees can apply for HRA Advance only in their first year of service.

2.  Employees are eligible to get HRA Advance for the number of months that succeed the month of application.

3.  The system will show validate these conditions and will show validation message in the loan application form where applicable.

- **Example scenario**: If an employee applies for loan in first month then employee is eligible for 12 months of HRA Advance as loan amount and no of instalment will be 12, if employee applies in 2nd month then eligible for 11 months of HRA Advance as loan amount and no of instalment will be 11 and so on.
>
- In order to implement this, an SP needs to be run in the HRW database.

### Future Leave Cancellation option to be available in ESS

#### November 2019  #17400

In HRW leave cancellation period can be set to **Future Days only**. When going to Leave cancellation in HRW only future leave are seen but in HRW plus back-dated leave are also available for cancellation. The client wants the **Future Days only** option in **HR Works Plus** as well.

In order to add this option, execute a query in the database.

Once the above script is executed the **Future date only** option appears in the **HRW Application Parameter >> Allowed Leave Cancellation Period** selection dropdown.

### Salary Transfer Validation with Loan Application

#### Oct 2019 -  #14211

If an employee has got Salary Transfer letter active then employee should not be allowed to apply Loan through ESS. Once the company gets the clearance letter employee shall be eligible for applying for loan. The validation must be based on loan types.

To implement the change, execute an SP.

![](/img/product-enhancement/image185.png)

### Approver should have Option to upload document in Airticket

#### Oct 2019 -  #16275

The client required an option to the approver to upload the document in ticket request screen from ESS.

To implement the change, execute a query to enable the application parameter.

![](/img/product-enhancement/image186.png)

### Option to exclude business leave type from leave application

#### Sep 2019 -  #16288

Currently, the client was applying for the business leave in ESS while entering the business claim. And the default leave type is configuring in the application parameter. Now the client required an option to deactivate the business leave type from the leave application.

Execute the query to implement the changes.

![](/img/product-enhancement/image187.png)

### Business Travel Plan

#### Aug 2019 -  #14584

A new feature was added in the business travel plan request and approval.

-   The Business travel plan request was integrated as paid leave days in payroll

-   In business travel plan request, an option to select the type of travel was introduced.

-   Provision to add multiple plans was included in the request screen.

-   Each plan should have options to enter the following details

    1.  Purpose of Travel -mandatory
    2.  From Place & To Place -mandatory
    3.  From date & To date with time -mandatory
    4.  Vehicle Type
    5.  Comments and attachment

For implementing this change, follow the steps below:

1.  Execute a query to enable the business travel plan menu

2.  Execute a query to activate Amount Handling Param In BT

3.  Execute a query to activate Amount Handling Param Values In BT.

4.  Execute a sql to work flow and sequence creation.

5.  A new parameter added in HRW + Application Parameters, 'Amount Handling In Business Travel Plan' (BTT-1, 100) with options Hide Amount/ Show Amount/ Allow Advance Request

![](/img/product-enhancement/image188.png)

### Business Travel Claim

#### Aug 2019 -  #14614

As per the client requirement some changes are made in the business travel expense screen.

The changes are listed below.

1. There must be an approved business travel plan for each business travel claim request.

2. A provision to select the business travel plan from the claim request screen.

3. While selecting the business travel plan, all the details entered in the plan must be filled with the claim request screen fields

4. Provision to edit these fields.

5. Provision to user to enter the actual amount of expenses.

6. The policy amount as per the grade of the employee is shown in the screen and option to enter the actual amount against each type with attachment option is provided. (not necessary for the request screen, but mandatory for approval screens)

7. The approver can view the policy amount and the actual amount.

8. The approver can edit the actual amount against each type.

9. The amount in the actual amount columns are required to paid to the employee after final approval.

For implementing this change, execute a query which enables the settings required for the changes.

![](/img/product-enhancement/image189.png)

### Maternity /Sick Leave Limit Scheme -- Calendar

#### Aug 2019 -  #14706

A following customisation was introduced in the annual limit checking period of sick leave and maternity leave.

-   Annual limit checking period of sick leave must be started from the first sick leave start date to one year.

-   Annual limit checking period should of maternity leave must be started from the first maternity leave start date to one year

-   It is required to keep the sick leave and maternity leave of each employee separately.

To implement this change, execute a script to list 'Based on First Leave Date Every Year' option in Annual Limit Checking period dropdown in Leave Limit Scheme screen.

![](/img/product-enhancement/image190.png)

### Leave Balance details in approval email

#### Aug 2019 -  #16092

A new column was introduced in the email to show the leave balance of the employee.

For implementing this change, execute a query.

![](/img/product-enhancement/image191.png)

### Option to print the L&C request details

### July 2019 -  # 15428

The client required to print the Letter and certificate instance form details and the approval details.

For implementing the change, execute a query.

![](/img/product-enhancement/image192.png)

### Show Sub Company Value on Prints

#### July 2019 -  # 9179

In some organisations, different companies are separated by using an entity named Sub Company. In such companies, the client required to show the Sub Company details while printing the Loan, General Expense and Air Ticket.

In order to implement the change, the parameter named 'Default Report Header Entity' in RPT-1 of HRW Application Parameters was introduced in the ESS also.

![](/img/product-enhancement/image193.png)

### Next Approver column in Leave Application

#### July 2019 -  # 12979

The client required to add a new column in the Leave Application Widget of Request, Approval and proxy tabs in the leave Application Screen.

![](/img/product-enhancement/image194.png)

### Loan Budget Feature

#### July 2019 -  #14583

A loan budget feature is introduced in the system. The loan budget amount of the organisation is saved in the system. The approvers have the option to view the loan budget amount and total outstanding loan amount of the company. While approving a loan request, approvers must check whether the existing loan budget of the company is enough to meet the loan amount. Thus, approver can determine whether to approve the request or to keep pending the request.

For implementing this change, follow the below steps:

1.  Execute a query to enable the Application Parameter. New parameters are introduced in the LOAN-1 Tab of Application Parameter which are 'Individual Loan Wise Limit' and 'Loan Budget Exceeding Validation'.

2.  Execute a SP for implementing the validation feature.

3.  Execute a query for enabling the parameter All Loan/Requested loan.

4.  Execute SP for showing the company budget details.

     ![](/img/product-enhancement/image195.png)

### Set Ceiling amount for General Expense request screen

#### July 2019 -  #15148

The organisations will have several types of general expenses and each expense will have ceiling amount. The HR admin have the provision to set the ceiling amount for each general expense. The client required to show validation when the expense exceeds the ceiling amount.

For implementing this change, a new parameter named 'Ceiling amount for General Expense' was introduced in the 'General Expense-2' tab in HRW + Application parameters.

Execute a query to enable the parameter.

![](/img/product-enhancement/image196.png)

### Option to edit the Ticket Cost

#### June 2019-# 15050

For the client, employees are availing the ticket as Company ticket. By default, the amount specified in the destination slab will be considered as the ticket cost. Unlike from HRWorks, there is no option to edit the amount and provide the actual ticket cost in ESS.

The following changes have been made by running a query in the DB in order to make the ticket amount visible and/or editable for either the requestor or for the approver or for both.

1.  A new Parameter '**Amend Air Ticket Availment**' is added onto the HSS-1 tab of the **HRW+ Application Parameters**. Users can select the following options as required:

    1.  Not Required

    2.  All Approvers

    3.  Final Approver

    4.  Air ticket amount should not be visible for Requester and for the Approvers who do not have Amend right.

![C:\Users\\Anandhu\\AppData\\Local\\Temp\\\\SnagitTemp\\Snag_39c9f616.png](/img/product-enhancement/image197.png)

### Option to print L&C before the final approval

#### June 2019-# 15233

The client needs to enable print option for Letters and certificates, even if the request is not fully approved. To enable this change a script needs to be run in the HR Works DB and then from the **HSS RPT** tab of the **HRW+Application Parameters** screen print option needs to be enabled.

![](/img/product-enhancement/image198.jpeg)

### Ticket Print Detail **June 2019-# 15267**

The client required to show the travel details such as From Date, To Date and destination details in the ticket request while printing from ESS.

For implementing this, execute a script.

![](/img/product-enhancement/image199.png)

*Fig: Print of Air Ticket Request with travel details*

### Option to escalate the request approval pending for time period

#### June 2019 -  #14371

Currently there is no option to escalate the pending approvals in HR Works or ESS. Hence a new feature to escalate the pending approvals is requested by the client. Now, in ESS, if an employee application request remains pending for approval for a long time, the employee can escalate the issue to the next approver.

For implementing this change, a new column is added in the 'Approval Workflow Settings' Screen named "Escalation Type".

Execute a query for enabling the column.

![](/img/product-enhancement/image200.png)

*Fig: 'Escalation Type' column in Approval Workflow Settings*

### ESS Leave Approval Change from/to/Resumption Date by HR

#### June 2019 -  #14486

The client required to edit From / To/ Resumption date of the employee's leave request by the supervisor dept head, HR officer and manager. Moreover, if the Resumption Date exceeds than the To Date, the leave days till the Resumption Date are considered as LOP. After the Resumption Date. Leave days are considered as Delayed Vacation.

For implementing this feature, a new parameter named 'Leave Type Between Leave To Date and Resumption Date' is introduced in the LV-1 tab of the Application Parameter.

Similarly, a new parameter named 'Amend Leave Application' is introduced in the HRW+ Application Parameters in which you can set who all can edit the leave application.

Execute a query for enabling the parameters.

![](/img/product-enhancement/image201.png)

*Figure: Leave Application screen with Resumption Date field*

### Employee Basic Info in All screen

#### March-2019 -  #14082

The client required to view the basic information of the employees such as division, department and designation in the leave application.

For this, a new tab named 'Profile' was generated in the Options next to the Workflow and Transaction history. Execute the query for activating this Profile tab.

![](/img/product-enhancement/image202.png)

*Fig: Leave application displaying employee details*

### Leave Encashment

#### April -2019 -  #14488

It was not able to edit the details regarding the leave encashment of the employees. The client required to edit the number of days and to approve the leave encashment through the off-cycle payroll.

1.  For this, two new parameters were added in the HSS 1 tab of the HRW + Application Parameter named 'Amend Leave Encashment' and 'Default Settlement method for Vacation Encashment'.

2.  The options included in the 'Amend Leave Encashment' are Not Required, All Approvers and Final Approver and the options included in 'Default Settlement method for Vacation Encashment' are Off-cycle payroll and Payroll sheet.

3.  Run the query for enabling these parameters.

    ![](/img/product-enhancement/image203.png)

    *Fig: HRW + Application parameter Settings for Leave Encashment through Off-Cycle payroll*

### Need to show the employee details in MISC request Print

#### March-2019 -  #14105

The client required to view the employee details while printing the MISC request.

1.  Execute the four views for displaying the details such as division, location, department and designation.

2.  Also, execute a query for getting the report ID.

    ![](/img/product-enhancement/image204.png)

    *Fig: MISC print showing employee details*

### Block Loan Application during Probation

#### March-2019 -  #14210

Initially, it was able to apply for loan for all the employees including the employees in probation. However, the client needs to block the loan application for the employees in probation.

For this, a new parameter named, 'Block Loan During Probation' was added in the LOAN-1 Tab of Application Parameter which is linked at loan level. When this parameter value is set as Yes, a validation message will appear while applying for the loan.

Execute a script for enabling the newly created parameter.

![](/img/product-enhancement/image205.png)

*Fig: New parameter in Application Parameter*

### OFF Day Validation

#### Feb-2019 -  # 13437

In HR Works Plus, there was no any validation with respect to the minimum number of days that is required between two off days while creating Employee Daily Roster. As per the client's requirement, a new option for setting a minimum gap between the OFF days of the employees is introduced.

For this a new Parameter named '**Minimum Days Gap Between Off Days**' is added in the **ESSTAM-1** tab of the **HRW + Application parameters**. Users can enter the required minimum gap between the OFF days as the value of this parameter.

![](/img/product-enhancement/image206.png)

*Figure: Setting 'Minimum Days Gap Between Off Days' in HRW + Application parameters*

#### Text in loan request screen

#### Jan2019 # 13397

As per clients request changes were made to display the following text in the ESS Loan Request page.

"I hereby accept the T&C of this loan"

### Leave Application

### Role delegation through leave for all employees

#### Jan 2019 # 13605 application

Currently Role delegation option in leave application is appearing for transaction approvers only. The client wants the Role Delegation details to be displayed for concerned employees as well in view only mode in the leave application record.

This is now implemented in the following manner:

A new parameter (Role Delegation Validation in Leave Request) is added to the **HSS-1** tab of the **HRW+ Application Parameter** screen in HRW.

Role Delegation option can be enabled for all employees by setting 'Mandatory At 1st Approval' as the value of **Role Delegation Validation in Leave Request** parameter. For all other selections, Role delegation in Leave Request will be displayed only for approvers.

### Leave Application template changes

#### Jan 2019 # 13492

In the leave application template view in ESS, the client wanted to have some new fields and to remove some other fields.

Two views are executed in the DB to make this change

### Role delegation through leave application

#### Dec 2018 # 11127

When role delegation request is placed with and through the leave application transaction, the leave request is approved first, then the role delegation request is sent to the delegate for acceptance.

The client needs to have control over the sequence in which the leave and role delegation requests are processed as follows:

When an employee places role delegation request with and through the leave application request:

1.  The delegate should accept role delegation request.

2.  Leave Request should be approved through work flow.

3.  If leave request is rejected by approvers, the role delegation should be cancelled.

To implement these changes, the following are done:

-   A new parameter (**Role Delegation Validation in Leave Request**) is added to the **HSS-1** tab of the **HRW + Application Parameters** screen. Users can set any of the following values for this parameter:

    -   **Not Mandatory**: When this is selected, approvers can approve the leave application transaction regardless of the acceptance or rejection of role delegation request.

    -   **Mandatory At 1st Approval**: When this is selected, it becomes mandatory that the role delegation request be accepted before the first-level approver can approve the leave application. The system displays a validation message whenever the first-level approver tries to approve the leave application if the role delegation is not accepted.

    -   **Mandatory At Final Approval**: When this is selected, all approvers other than the final-level approver can approve the leave application regardless of the status of role delegation request. But it becomes mandatory that the role delegation request be accepted before the final-level approver approves the leave application. The system displays a validation message whenever the final-level approver tries to approve the leave application if the role delegation is not accepted.
        \
        **Note**: In case if the role delegation request is not accepted or rejected by the delegate, the leave application too will be rejected.

![](/img/product-enhancement/image207.png)

### Hide Save option in Leave Application

#### Dec 2018 # 12963

The client does not require the Save option for leave application transactions in ESS. Users should only be able to submit a leave application for approval after providing the required information.

To get this working, a new check box field (**Allow to save Leave Applications**) is added to the **ESS Leave Parameters** tab in Leave Master. ESS users can save the leave application form only if this check box is selected. Otherwise they won't be able to save the form but can submit the form for approval or cancel it.
\
![](/img/product-enhancement/image208.png)

#### Option to print leave application before approval

##### Nov 2018 -  # 11762

The client wanted to print the leave application record from ESS in a specific template. This allows all approvers to take print out of leave application before final approval.

For this, an REPX file was created and it is then imported into the Custom Report Designer in HR Works. The user can now modify the template as required from the Custom Report Designer menu in HR Works.

#### Attachment Option for Approver in Leave application

##### Nov 2018 -  # 11760

The client wanted to have attachment option on leave application so that approvers can attach files while approving/ rejecting the leave application of employees. For this, a new parameter '**EntityID (User Defined Fields For Leave Approval)**' is added to **HRW+ Application Parameter >> HSS-1** tab. Users can create user defined field group on the Card Types >> User Defined tab where they create and define fields and their data types and then set that user defined field group as the value of **EntityID (User Defined Fields For Leave Approval)** parameter**.** Refer to the screenshots below**:**

![](/img/product-enhancement/image209.png)

The above image shows the user defined fields, which are created on the **Card Types >> User Defined** tab**.** Note that three Attachments fields viz Attachment, Attachment2, and Attachment3 are created with 'File' as their data type.

![](/img/product-enhancement/image210.png)

The above image shows how the user defined field group that was created on the Card Types screen is set as the value of **EntityID (User Defined Fields For Leave Approval)** parameter in **HRW+ Application Parameters >> HSS-1** tab.

Once the above settings are saved, the approvers now will have the above-mentioned user defined fields on the leave application form. See the image below:

![](/img/product-enhancement/image211.png)

#### Sick Leave Settlement changes

##### Oct 2018 -  # 11531

When an employee selects the leave type on the leave request form in ESS, he/ she can view his/ her leave balance and related information as follows:

1.  Clicking on the **View Details** link will show the leave balance and related information in a popup window as follows.

![](/img/product-enhancement/image212.png)\
**Note**: The **View Details** link appears after the employee selects the leave type from the **Leave Type** drop down field.

Before implementing the changes requested by the client, when employees apply for any leave such as sick leave for which leave limit scheme is set, they can only view the split up of leave days (Entitled, availed, and balance) based on the previous leave requests. The data shows was not updated on the leave data (From date, To date, and/ or number of days) entered on the current leave request form.

A new parameter (**Show Leave Days Split up for Leave Limit Scheme**) is introduced in the **ESS-1** tab of the **ESS Application Parameter**. Users can set the parameter value to **Yes** or **No** which tells whether to allow the employees to view the split up of leave days (Entitled, availed, and balance) against the days of leave specified in the leave form.

When set to **Yes**, employees view their leave balance against the applied leave day(s) as well as the split up of leave days based on the leave limit scheme as follows.

-   In addition to the **View Details** link, a new link (Leave Days Split Up Details) appears on selecting the leave type.

-   Clicking on the **Leave Days Split Up Details** link will show the split up of leave days against the number of leave day the employee has specified in the current leave request form. This means that the leave split up details are updated dynamically based on the Leave Type, From date and To date selections made by the employee.

![](/img/product-enhancement/image213.png)

#### Select All Option in Leave Return

##### Oct 2018 -  # 10910

On the **Proxy** tab of the **Leave Return** screen, from the **Select Employee** dropdown above the **Leave Application Pending Return Date** grid, users had to select the employees for whom he/ she is acting as proxy so that the **Leave Application Pending Return Date** grid lists the leave application records (without return date) of the selected employee/s. As per the new requirement this selection is removed and instead of that the **Leave Application Pending Return Date** grid now lists all leave application records (without return date) of all employees for whom the logged in user is acting as proxy.

### Change in miscellaneous application

#### Oct 2018 -  # 11385

Based on the client's requirement, miscellaneous application menu in Works plus need to be redirected to a new link in a new tab. That is, when the user clicks on the miscellaneous application menu, a new tab needs to be open instead of the standard menu.

### HSS-HLB/170d-Amendments in Loan Application

#### July 2018 (#7782)

Payment History which was not present earlier is now shown as a last widget in the Loan Application screen, similar to those in Business Travel and General Expense. Now, the Employee can view when the loan has been disbursed, type of Settlement Method like Payroll/Off cycle/Outside and the Bank information too. Same is done in Approval & Proxy- Employee Record Snapshot.

### HSS-TKF/05 - Revoke the Delegated Role

#### July 2018 (#7333)

Earlier in Role delegation we have options such as Submit and delete only.

Based on client requirement new feature is added in Role Delegation as --

-   Revoke: This option can be used to cancel a role delegation transaction performed.

-   Change To Date: This will update the "To Date" in an existing role delegation transaction.

-   Change From Date. This will update the "From Date" in an existing role delegation transaction.

These options are made available in the bulk action drop down. Once the action is selected, only those transaction will be filtered out which is fully approved and accepted by the Delegate.

![](/img/product-enhancement/image214.png)

### Annual Leave and CME Leave Validation

#### July 2018 (#10641)

On applying for AL or CME leave certain validations need to be checked with in a Branch and within a Category. Validation should be shown for following scenarios:

-   Annual leave **needs** to be applied at least 2 weeks in advance of leave start date else leave application to be blocked. **Similarly,** CME leave needs to be applied at least 1 week in advance.

-   Doctors can only apply for Annual Leave maximum of 14 calendar day at a time.

-   Not more than 3 doctors can **avail** AL or CME on a **same** day.

-   Rule for doctors to take leave during same period is

    -   1st doctor who applies can take 14 days.

    -   2nd doctor who applies in the same period can only take 7 days.

    -   3rd doctor who applies in the same period can only apply less than 7 days.

-   The rule applies only if the leave falls on same date.

-   The Validation shall be applied within a Branch within a category.

-   The rule shall be only applied for specific category of employees.

-   The rule shall not be applied during off peak days. i.e. June last week to Sep first week and Dec last week to 1st week of Jan.

**The solution approach is done in the following manner:**

-   Five new parameters are added in T00-1 tab in HRW + Application Parameters

-   AL Application Advance Period (Days) Parameter to validate AL request to be applied before a specific period.

-   Max. Leave Days for 2nd staff Application

-   Max. Leave Days for 3rd staff Application

-   (2 & 3 determines the rule to be applied for various applications)

-   Off Season for Leave Validation. A season can be set for which the rules to need not be applied.

Parameters to be set through Application **Parameters (**Multiple Entity). Validation to be applied through back end additional validation function while applying the leave.

![](/img/product-enhancement/image215.png)

### Export option in Time Sheet Requests

#### July 2018 (#11087)

Following option are introduced in the Time sheet Menu.

-   Export to Excel option is implemented in the following Widgets of "Request, Approval and Proxy" pages- Employee List, Time Sheet Requests and Payment History.

-   In the Request, Approval and Proxy Page "Next Approver Column" is added in Time Sheet Requests Widget.

![](/img/product-enhancement/image216.png)

### Print option in ESS screen

#### July 2018 (#9741)

Print options in ESS screens are achieved by introducing a new tab HSSPRT-1 in HRW + Application Parameters with values as 'None/All Stages/ Approved Only' (tab). If the parameter is set to None, print button to be made hidden. If it is set as All Stages, print to be enabled for all Stages and if it is Approved only, print to be enabled only for approved records.

Print option in main screen for Leave Application screen has been removed.

![C:\Users\\LENOVO\\AppData\\Local\\Temp\\SNAGHTMLef00806.PNG](/img/product-enhancement/image217.png)

## Leave and Accrual

### Indemnity Scheme in the web applications

#### April 2021 -  #21444

A new screen "Indemnity scheme" was developed in web application.

In order to implement the new feature, execute the query to enable the menu.

![](/img/product-enhancement/image218.png)

### Ticket fare in the web application

#### Jan 2021 -  #21204

Developed new screen ticket fare in the web application that helps to set destination, fare and age.

In order to implement the new screen, execute a query to enable the menu.

![](/img/product-enhancement/image219.png)

### Leave master in the web application

#### Jan 2021 -  #21202

A new screen 'Leave Master' was developed in web application for creating different types of leaves.

In order to implement the change, execute a query to insert the menu.

![](/img/product-enhancement/image220.png)

## Payroll Management

### Currency Master in Web Application

#### April 2021 -  #21199

A new screen "Currency Master" was developed in web application.

In order to implement the new feature, execute an SQL to enable the menu.

![](/img/product-enhancement/image221.png)

### Paycode Master in the web application

#### Jan 2021 -  #21201

A new screen 'Paycode Master' was developed in web application for creating different types of paycodes.

In order to implement the change, following steps were executed:

-   Execute a query to insert the menu.

-   Execute three queries to enable the module 'Pay Code Master' in ESS.

![](/img/product-enhancement/image222.png)

## Clearance Form and Exit Interview

### Sequence order for Clearance form sub activity

#### Oct 2019 -  #16430

The client required to process the clearance sub activities in the sequence order given below.

1.  Department Sub Activity

2.  IT Sub Activity

3.  HR Sub Activity

4.  Auto Garage Sub Activity

5.  Finance Sub Activity

At 1st clearance form should be sent to Department Head (Department Sub Activity) once approved from Department, it should flow to IT Manager (IT Sub Activity) and so on.

To implement the change, execute a query to enable the application parameter.

![](/img/product-enhancement/image223.png)

### Ability to see all clearance form request by other users

#### May 2019 -  # 15145

Currently the clearance form could be seen only by the requested User. However, the user required to view and print all the clearance request to all the users.

For this execute a query for enabling HRW+ Application parameter.

### Vacation Clearance Form and Automatic Initiation

#### May 2019 -  # 14581

The client required to automatically initiate the clearance form during a vacation request approval. The clearance form will be initiated after the final approval of the vacation request.

For this a new parameter named 'Auto Initiate Clearance Form On Vacation Final Approval' was introduced in the HSS-1 Tab of the HRW + Application Parameter. When the parameter is set to 'Yes' the clearrance form is automatically initiated.

Execute a query for enabling the parameter.

![](/img/product-enhancement/image224.png)

*Figure: HRW + Application Parameter Screen*

### Asset Details link to Clearance form

#### May 2019 -  # 14212

The client required to track the Employee Asset Details such as Asset Name, Asset No etc through the Employee Personal Records. When an employee gets separated, the clearance form must list the assets the which the employee had, but not returned. The assets in which the return date is null will be listed.

For this Create assets in Card Types and set values in Card Master.

Create Different assets for checklist in the Personal Records fields of Card Types, further create corresponding parameters under each asset and set the values for the employees.

Execute a view for getting the details regarding the assets.

![](/img/product-enhancement/image225.png)

*Fig: Clearance Form showing Asset Details*

### Provide Print option in all level of request and approval

#### May 2019 -  # 15150

All the request and approval screen contain the option to Print the form in all levels. Hence the user required an option to print the Clearance form in all the request and approval screen.

Execute a query for enabling the clearance form in all request and approval screen.

Set the HRW + Application Parameter for printing.

![](/img/product-enhancement/image226.png)

*Figure: Clearance Form*

## Time and Attendance Module

### Default the Permission Type in Regularization Request

#### Jan 2021 -  #20710

Presently, while creating new attendance regularization request in ESS, it is required to select the permission type even if it contains only a single defined permission type in the system. Hence, now, the client required a provision to set the value default in Permission Type while creating a new Attendance modification request.

![](/img/product-enhancement/image227.png)

### Team View in Attendance Regularization Approval Tab

#### July 2021 -  #22335

The client required to display the Team's Attendance information such as the Team View in Leave Application. An option to configure the required criteria will also be added. Only the selected criteria will be listed in the Team View which is Present Days, Leave days, Absent, Late In, Early Out, Holiday and Reprocess.

In order to implement the functionality, execute an xml.

![](/img/product-enhancement/image228.png)

### Showing Exceptions Based on occurrence In Consolidated Attendance Report

#### Feb 2020 -  #18152

The client required to show to exceptions in consolidated attendance report based on occurrence of Late IN, Early Departure, Missing IN/OUT and Absent.

For example, if the employee is Absent for n days during the selected period, those records only should be filtered in the report. The value n should be accepted as a parameter. Some option to select whether it is consecutive or non-consecutive should be available.

In order to implement the change, do the following.

1.  Execute a report template.

2.  Execute an xml file

3.  Execute four queries to enable the filter option.

4.  Set Report Template for Standard, Late IN, Early Departure, Missing IN/OUT & Absent Select Filter Options (Late IN, Early Departure, Missing IN/OUT & Absent)

![](/img/product-enhancement/image229.png)

### Monthly Roster Option

#### Feb 2020 -  #15503

In HR Works Plus a new feature was added for setting up the monthly roster. An option was provided for selecting the employee, Month and Year for the Monthly Roster. Once selected, you can select the weekly roster for each week in the selected month and submit the request. An option for Approval workflow was also included.

In order to implement the change, do the following.

1.  Execute the query to enable menu status.

2.  Execute a query to enable sequence control and workflow settings.

3.  Execute the query to get Employee Monthly Roster screen menu under Applicable Menus.

![](/img/product-enhancement/image230.png)

![](/img/product-enhancement/image231.png)

### Displaying shift details in view calendar screen

#### Dec 2019 -  #17927

The client required to view the shift code and description for each day as a tool tip in the corresponding cell in view calendar screen. The shift details for future dates also should be shown. If the employee has got OFF/Leave/Public Holiday for future dates, that also should be shown in the corresponding cells (not as tool tip).

To implement the change, execute a query.

![](/img/product-enhancement/image232.png)

### Doing attendance calculation for nearby dates also for records from TAM_AutoCalcLogs table

#### Dec 2019 -  #17928

Currently posting is done for the single date only. instead of this, the client required to do posting for -n to +n days. The previous days and future days should be accepted as a parameter in Application parameters.

Currently when the shift for a date is changed or when time bookings is added, a record will be written to TAM_AutoCalcLogs for automatic posting.

To implement the change, 2 new keys was added in appConfig which are  AutoCalcStartDay to consider previous days and  AutoCalcEndDay to consider future days in attendance calculation.

### Cancel Approved Requests in Attendance Regularization

#### Dec 2019 -  #16351

The client required an option to cancel the approved permission requests in Attendance Regularization. In case, the user has used the allotted break buffer defined against the permission type, the deducted minutes should be returned back to the break buffer upon cancellation.

To implement the changes, follow the steps below.

1.  Execute a query to enable the cancellation checkbox.

2.  Execute a query to enable the workflow of break permission cancellation.

![](/img/product-enhancement/image233.png)

### Weekly Schedule settings - Entity wise option in HR Works Plus

#### Dec 2019 -  #16719

The client required an option in HR Works Plus to set entity wise weekly roster and should have workflow approval.

![](/img/product-enhancement/image234.png)

### Template download option for Shift Schedule Upload

#### Nov 2019 -  #17407

As per the client requirement download option for shift schedule upload excel was introduced in the employee daily roster screen.

![](/img/product-enhancement/image235.png)

### Issues in Check In/Check Out button in view calendar screen.

#### Nov 2019 -  #17308

The client required the following changes in Check In/Check Out option in view calendar screen

-   Currently, if Check In/Check Out is done, the transaction is not updated in Time Bookings, which must be changed.

-   Currently, after submitting the request, the text  Missing IN/OUT or Regularization is shown in the corresponding cell. Instead of this, system should allow to create a separate type and show in the cells, or else the cell can be shown as blank.

-   Currently the Check In/Check Out button is available to all employees; this should be configurable at employee/entity level.

To implement the change, execute a query to enable application parameter.

![](/img/product-enhancement/image236.png)

![](/img/product-enhancement/image237.png)

### Comments in Attendance Regularization

#### November 2019  #16480

The client wants to make the Comments as a mandatory field in the Attendance Regularization request screen. To implement this, the user defined field settings for comments is removed and a query is executed in the database.

### To add new field in the attendance regularization screen

#### November 2019 # 17332

Changes have been made based on the client's requirement that in the **Attendance Regularization** screen, it should show the total number of employees whose attendance regularisation task is assigned to the logged in user. This should appear next to the **Select Employee** dropdown against the label **Total Employee Count**.

### New Daily Shift Schedule Screen

#### Oct 2019 -  #16064

The client required the below pinpointed requirements to manage the employee daily roster and leaves.

1.  Based on the month selected by the User, the Employees should be listed on the screen based on the Users Access rights. If the roster for the Employee is already assigned, the corresponding shift code or the leave type should be listed against the respective days on page load.
    A drop-down in the screen should list the Shift Code masters and the Leave Masters. The assignment process must be as follows.

-   Select a Shift/Leave Type from the drop-down list.

-   To assign the shift for a day, click on the respective Date Cell.

Each shift code will be tagged with colour and the same colour will be applied to the date cell upon clicking the cell.

1.  Two off days in the same week should be blocked, however additional days off are allowed but should be listed as "Authorized Leave"). A parameter should be set in the application which would specify the minimum no of days to be worked after the last off day. Further, provide an option to link the functionality at the Employee and Entity level. An Option to lock the rostering for previous months. However, the last 7 days of the previous month could be edited.

2.  Validation must be added to check if the off-days are assigned in case the employees are on continuous leave. The application should block the request submission if the off days aren't assigned.

To implement the change, follow the steps below.

1.  Execute a SQL to enable the functionality.

2.  Execute a query to set the length of the value of the data return.

3.  Execute a SQL to lock the rostering for previous months

![](/img/product-enhancement/image238.png)

### To add Employee work duration feature in attendance regularisation

#### Oct 2019 -  #15111

The client required an option to see the time an employee worked and half day Leave type for the day in Attendance regularisation.

![](/img/product-enhancement/image239.png)

### Disable Click Events in Attendance Regularization

#### Oct 2019 -  #9019

Client required to view the attendance information in Attendance Regularization screen and doesn't require the regularization functionality. Further needs to disable the click events if the approval workflow is set to none. Currently, it notifies the user to set the approval workflow.

Further the Transaction grid listed below the attendance calendar needs to be hidden. (Eg: Hide the list if there aren't any transaction at all).

To implement the change, follow the steps below.

1.  Execute an XML.

2.  Execute a query.

### Absent Report in HR Works Plus

#### Oct 2019 -  #16716

Absent report was added in HR Works Plus.

To implement the change, execute a query.

![](/img/product-enhancement/image240.png)

### Additional Functionalities in Daily Roster Screen

#### Oct 2019 -  #16663

The client required some additional functionalities in the daily roster screen which is explained below.

-   Week wise frame in Roster screen: The start day of each week needs to be highlighted with different colour. Please provide an option to select the Start day and link this at Entity and Company level.

-   List Public Holidays in different colour

-   Option to Export the data.

-   Rostering period to be set as 21-20 of each month: Users should be restricted from editing the data before and after this period. However, an admin user must be able to override the roster during the locked period.

-   Users should have an option to assign their own roster (In case the Employee Master Assignment is set to Approval Workflow).

To implement the change, follow the steps below.

1.  Execute a query to enable extended access right option.

     ![](/img/product-enhancement/image241.png)

### Changes in the consolidated attendance report

#### Oct 2019 -  #17140

The client required a new column in the consolidated report which show the remarks in the regularizing request.

In order to implement the change, execute the report template.

![C:\Users\\USER\\Desktop\\not showing.png](/img/product-enhancement/image242.png)

### Buffer Time for Break Permission

#### Oct 2019 -  #16350

The client required an option to set a buffer value for the break permission. The break permission requests should be blocked when the user has exceeded the allowed limit. If a user applies for 60 mins permission, the value should be deducted from the break buffer. The buffer limit should be a configurable parameter as this value would differ for each permission type.

Further, an option to limit the maximum applicable duration for each day was also included. (Configurable parameter against the permission type)

In order to implement the change, execute a query.

![](/img/product-enhancement/image243.png)

### Monthly Close Option

#### Sep 2019 -  #15507

A new option was introduced to set up the monthly close. Once monthly close is done with a specific date, the attendance transactions such as Time Bookings Entry, Weekly, Daily, and Monthly Shift Assignments and Leave application, before the monthly close will be validated and blocked from the system.

To implement the change, a new parameter named 'Transaction lock period' was introduced in the TAM-1 tab of the Application parameter.

Execute an xml to implement the change.

![](/img/product-enhancement/image244.png)

### Entity wise filter option in Daily Roster, Weekly Roster and Monthly Roster

#### Sep 2019 -  #15502

An option to filter the Daily roster, Weekly roster and Monthly roster on the basis of the Personal and Positional entities is introduced in HR Works Plus.

![](/img/product-enhancement/image245.png)

### Duplicate Time Bookings validation

#### Sep 2019 -  #15510

Currently, the system does not validate the duplication of the time bookings. Hence, a validation was introduced in the duplication of the time bookings.

To implement the changes, two new parameters was introduced in the ESSTAM-1-tab of the HRW+ Application Parameter.

Execute xml to implement the changes.

![](/img/product-enhancement/image246.png)

### Attendance Logs (Check in and Check Out) bulk upload.

#### Sep 2019 -  #15508

The client required the provision to upload the Check In and Check Out Logs though Excel template and an approval workflow for the same.

Execute xml to implement the changes.

![](/img/product-enhancement/image247.png)

### List Total Hours Worked in Attendance Calendar

#### Sep 2019 -  #16354

The total hours worked during each day was not noted in the attendance calendar. Hence the client required to list the total hours worked by the user for each day in the Attendance Calendar Date Cell.

Execute an xml file to implement the changes in the attendance calendar.

![](/img/product-enhancement/image248.png)

### Break Information in Attendance Calendar

#### Sep 2019 -  #16281

The following changes are introduced in the Attendance Calendar

-   Listing the break information in Attendance Calendar (Break Out-Break In).

-   When the break duration is greater than 60 mins, the text is highlighted in red.

-   An option to validate the breaks to users. Once validated, the text colour change from red to black and only then the Validate button will be visible on the screen.

-   An exception shown in cases where the Break-Out or Break-In is missing.

To implement the changes, execute an xml.

![](/img/product-enhancement/image249.png)

### Option for defining employees to multiple groups and assigning the permission for the selected group to work on public holiday

#### Sep 2019 -  #15127

A provision to set the permission for employees to work on public holidays is introduced. If an employee who doesn't have the permission to work on Public holidays work on a public holiday, the system will not calculate the attendance for them.

New options are added in the 'OT Pre-Approval Requirement Parameter For' parameter in ESSTAM-1 tab of HRW+ Application Parameter.

Execute a query to implement the changes.

![](/img/product-enhancement/image250.png)

### Option for break permission on selected week days between a data range

#### Sep 2019 -  #15126

The client required to set the permission for selected week days between a data range in break permission.

A new field named 'Applicable Days' is added in Attendance Regularization Screen in break permission section.

Execute a query to implement the changes.

![](/img/product-enhancement/image251.png)

### Overtime and Weekly Total Hours in Attendance Calendar

#### Sep 2019 -  #16272

The client required to view the overtime hour worked by the employee in the respective date cell of the Calendar. Moreover, the total weekly hours worked by the employee must be displayed against each week in the calendar.

Execute an xml to update the changes.

![](/img/product-enhancement/image249.png)

### Overtime Request

#### Aug 2019 -  #14741

The client required a new overtime request functionality which is a pre-approval process for getting OFF day overtime.

1. Employee's OT request/approval is only for Weekly off days.

2. If the employee has requested to work on Off day, then he/she, must get approved his OT request through self-service. Then he/she will be eligible for OT on off day. Otherwise, the system will reject the employee's off day OT.

3. All other days OT is calculated without any approval. (Normal day OT)

4. The supervisor can request on behalf of his employees (Proxy).

For implementing this feature, the following changes are made.

1.  A new parameter, 'OT Pre-Approval Requirement For' was added in the ESSTAM-1 tab of the HRW+ Application parameters.

2.  Execute a query to enable the screen.

![](/img/product-enhancement/image252.png)

### OT Upload Screen

#### Aug 2019 -  #15290

The client required a provision to upload the overtime hours into the application. After uploading the OT hours, the data is populated into the screen. From the screen, the user could edit the uploaded content, filter the data based on entity and date, list out the actual OT worked by the employee against each entry and export/print the data. The uploaded overtime should be considered only if the data is less than the actual OT, i.e. whichever is lesser should be taken into payroll. These data should be reflected in the attendance reports.

For implementing this change,

1.  Execute a query to enable the application parameter.

2.  Execute a query to enable the upload option.

![](/img/product-enhancement/image253.png)

### Attendance/OT Approval

#### July 2019 -  #14576

A new feature to approve the Attendance/Overtime/Shortage of employees by the supervisor till the last approver to get it paid through payroll is introduced. If it is not approved, the OT or Attendance are not considered for payroll calculation.

For implementing this change, follow the below steps:

1.  Execute a query for enabling the screen

     ![](/img/product-enhancement/image254.png)

### Option for setting up the Daily and Monthly Limit for Excuse hour

#### July 2019 -  #15125

The client required an option to set up the Daily and Monthly limit for Attendance regularisation request.

Validation options are included in the Attendance Regularisation screen for break permission.

For implementing the change, execute a query.

![](/img/product-enhancement/image255.png)

### OT Approval Screen

#### June 2019-# 13438

The client requires that the OT Approval screen similar to the one in eTAS should be added in HR Works Plus also. An option for converting the OT hours to Compo OFF should be added in this. If the employee applies for Compo OFF through the View Calendar Leave entry option, the balance against the accrued Compo OFF should be validated.

The following needs to be done in order to get these changes:

1.  A query needs to be executed in the HRW DB

2.  Create wage type for present days and set the **Wage Type Present Days** parameter on **Application Parameter >> TS-1** tab.
    ![](/img/product-enhancement/image256.png)

3.  After final approval from ESS; wage type-based time sheet batch will be created for each employee in submitted status, which in further needs to approved from HR Works wage type base time sheet screen.

### Availed Weekly OFF Count in View Calendar

#### March-2019 -  #13545

In the Attendance Regularisation screen, in the Time and Attendance Module, the calendar view of the details was included in the screen. Now the client required to add the details of the availed weekly off details in the screen.

Therefore, the number of Availed Weekly Off till the selected month is included in the Attendance Regularisation screen.

![](/img/product-enhancement/image257.png)

*Fig: Attendance Regularisation screen displaying Availed Weekly Off*

### Automatically create break permissions in view calendar

#### March-2019 -  #13431

In the Attendance Regularisation screen, Break Permission was enabled manually by the employees. The client required to automatically create the break permission the view calendrer screen.

1.  A new parameter is added in the HRW + Application Parameter in the ESSTAM-1 tab named 'Enable Auto Break Permission'.

2.  Run a query for enabling the Parameter.

3.  When 'Enable Auto Break Permission' parameter is set to true, a new button named 'Create Break Permissions' appears in the Attendance Regularisation screen near the 'Add New' button.

4.  While selecting the 'Applicability' as Break Permission, the break permission records will populate automatically.

     ![](/img/product-enhancement/image258.png)
>
    Automatic Break permission button in Attendance Regularisation Screen*

### Shift Search Option

#### Feb-2019 -  # 13436

In ESS, users were not able to search the **Shift List** grid for shifts with shift parameters/ data such as such as sift code, description, shift start time and shift end time on the **Employee Daily Roster**. This functionality is introduced as per the requirement by the client.

Now a new parameter (**Show Shift List in Employee Daily Roster**) is added in the **ESSTAM-1** tab of the **HRW + Application parameters**. If the value of this option is set As True, then the shift search Option will be enabled in the **Shift Lists** grid on the ESS **Employee Daily Roster** page as follows.

![](/img/product-enhancement/image259.png)

*Figure: Shift list in the Employee Daily Roster*

### Shift Schedule Upload

#### Feb-2019 -  # 13433

In the Daily Roster Grid, the option for assigning the shift schedule for each day were provided, but assigning the shift for each working hour was not present. The client required the option for assigning the shift for each hour through an excel sheet. The steps for the same is explained below.

In the **Daily Roster** grid of the **Employee Daily Roster** page, a new combo box named '**Roaster Type**' is added.

1.  While selecting 'Shift Schedule Upload', select one Date and Upload the created Excel File.

2.  The shift timings are entered in the Excel sheet in the **xlsx** format.

![](/img/product-enhancement/image260.png)

*Figure: Updated Daily Roaster Screen*

![](/img/product-enhancement/image261.png)

*Figure: Excel Sheet of Shift Schedule Upload*

### Time sheet

#### Feb-2019- ##13330

The client required employee timesheet option which they didn't have in the ESS. They also need the following customizations in their timesheet:

-   Employees require to fill their Time sheet for a month based on which Cost Allocation report will be generated.

-   Validation and conditions are require for the data entry.

To implement this,

1.  In the HR Works, in the **TS1** tab of the Application Parameter Menu, a new selection named '**Time Sheet Entity 4'** is added.

2.  In the **ESSTS-1** Tab, make the appropriate selections.

3.  In the **Wage Type Master** screen, create the Wage Type for Full Day, Half Day, Leave, Holiday and Off Day.

4.  A query needs to be executed in the DB by specifying the Wage Type ID.

5.  In the **TS-1** tab of **Application Parameter**, the corresponding value for Wage Type of Weekly Off and Public Holiday should be specified.

6.  Execute the [script](https://dli-apps.sourcerepo.com/redmine/dli/attachments/download/38138/AppParamEnablingQry.sql) to enable the following two parameters in the **Application parameter** >> **ESSTS-1** tab and set the corresponding value to them.

7.  In ESS, **HR Portal >> Report Centre**, Run the Time Sheet by entering the From Date and To date.

![](/img/product-enhancement/image262.png)

*Figure: Time Sheet Report*

### Cost Allocation Report

#### Feb-2019- ##13332

Refer to Redmine ticket##13330 to know more about the particulars of time sheet mentioned in this section.

The client requires the cost allocation report generated for the employees from the data entered in the Time Sheet.

To implement this,

1.  A query needs to be executed to enable the menu.

2.  Select the Cost Allocation Report, Enter the From Date and To Date. You can run the report, clear the report, and save the report.

     ![](/img/product-enhancement/image263.png)

*Figure: Cost Allocation Report*

### View Calendar Request Validations

#### Jan2019 # 13432

The client requires that the following validations be added in View Calendar Request transaction while submitting Break Permissions/ Regularization requests. These validations should be enabled or disabled based on some flag in the **Request Master**.

• Missing IN/ OUT request should not be submitted for Absent Days

• If the selected date is back dated and attendance status of that date is "Absent" or "ReProcess", Break Permission request should not be submitted. The existing option of submitting break permissions request for future dates should be available.

### Auto Marking

#### Jan2019 # 13429

Client wants to show Absent entries in View Calendar as Auto. For this, a new parameter (**Auto Marking**) is added in **HRW+ Application Parameters** (**ESSTAM**-). Users can select True/ False. If set to True, all absent entries in View calendar will be marked as Auto.

Users can set this parameter at entity-level and employee- level.

### Hiding Day Type in View Calendar

#### Jan2019 # 13430

As per the client's requirement, In **View Calendar Request \- Break Permissions** transaction form, "Start Day Type" & "End Day Type" should not be visible if both the Start Day Type and End Day Type configured in **Shift Master** is "Today". If the employee is trying to select future dates, either daily, weekly or company level roster should be considered for finding out the shift code for the day.

For this, a new checkbox field (**HideDayType)** is added in **Attendance Permission Types** screen, under the **Break Permission Parameters** section. If this checkbox is selected for any permission type, Start Day Type & End Day Type fields will be hidden in the break permission area.

### Color change for modified transactions in attendance regularization

#### Jan 2019 # 13744

The **Approval** tab page of the **Attendance Regularization** page shows all original transactions as well as the modified ones. The client required that the font color of all modified transactions needs to be changed to red so that the users can distinguish between the original transactions and modified transactions.

### Executive Dashboard

#### July 2018 (#6251)

An executive dashboard is designed as per user requirement. By default, the record is filtered on the basis of location. Sample Executive Dashboard will be as shown below:

![](/img/product-enhancement/image264.png)

It consists of fields like Required Hours, Total Hours, Regular Hours, Total Wasted Hours, Overtime Hours, Approved Hours, Violation Hours, Absent Employees, Late IN Employees, Early OUT Employees, Approved Requests, Rejected Violations, Productivity, Violations.

### To have an option to export the transaction list data to excel from Attendance Regularization screen

#### July 2018 (#10831)

As per the requirement, an export option is implemented in proxy tab of attendance regularization screen for the transaction list widget and added next approver column in the grid.

![](/img/product-enhancement/image265.png)

## General

### Cross company approval in ESS

#### Feb 2020 -  #17509

The client required the cross-company approval feature in ESS. It must be possible to approve other company user's request from the parent company of the approver in the transactions such as Leave application, Loan Application, Business travel, Benefit claim, Air ticket, General Expense and Clearance form.

In order to implement the change, execute a query and set  Cross Company Approval in 'Show To Do List' parameter in HSS-1 tab.

![](/img/product-enhancement/image266.png)

### Showing Employee Entities in request and approval screen

#### Nov 2019 -  #15144

The client required to show Employees Entities in request and approval screen & reports.

To implement the change, execute a query to enable the visibility of employee entities in screen and reports.

![](/img/product-enhancement/image267.png)

![](/img/product-enhancement/image268.png)

### Option to edit all requested data in all approval level

#### Nov 2019 -  #15149

The client required an option to edit all requested data in all approval level. This option should be available in leave request, leave return request, General Expense and Travel Expense requests screens.

To implement the change, execute a query.

![](/img/product-enhancement/image269.png)

![](/img/product-enhancement/image270.png)

### Block user from using mobile app on multiple devices

#### Oct 2019 -  #16956

Earlier, when the mobile app is configured on a second device by entering the admin credentials, the user will be still able to use the app on the initially configured device. This feature was blocked in the new system.

### Export option to XLS File

#### Oct 2019 -  #13889

The client required the export option file format in xlsx rather than pdf.

In order to implement the change, execute a script for enabling xlsx export option in all reports.

![](/img/product-enhancement/image271.png)

### Show Last Approver Comment

#### Aug 2019 -  #15944

Currently the last approver comment was shown in the transaction history. Now, the client required to view the last approver comment in the approval screen. Similarly, the previous approver comments need to view in the main popup screen while approving a transaction.

For implementing the change, execute a query.

![](/img/product-enhancement/image272.png)

### ESS Workflow Escalation

#### Aug 2019 -  #14616

The client required a workflow escalation feature in HR Works Plus.

If a request is pending with a user for a specific number of days, the following actions should be taken automatically by the system

1. Provide a provision to enter the number of days to start the escalation task\
2. A reminder email must be sent to the approver after some specific number of days to the approver and other higher authorities (CC to next approver & HR manager) referring that the requests are pending to approve.
3. After the specified number of days, the request should be auto-approved and forward to the next approver with usual notifications and CC email to the HR Manager.

To implement this change, execute a query.

![](/img/product-enhancement/image273.png)

### Auto Rejection Option in Workflow Escalation

#### Aug 2019 -  #16018

An auto-rejection feature is developed in workflow escalation. The two types of auto rejection developed are

1.  Auto rejection with notification

2.  Auto rejection

In a workflow with auto-rejection with notification, after the specified number of days the notification will go to the concerned approval authority and CC to the HR manager and reject the request after the specified number of days.

In a workflow with auto-rejection, after the specified number of days the request is rejected.

To implement the feature, execute a query to activate Workflow Escalation.

![C:\Users\\D L I\\Desktop\\a1.png](/img/product-enhancement/image274.png)

### Restrict the attachment size in all request page to 100 KB

#### July 2018 (#6350)

Normally the attachment is ESS can hold size less than or equal to 8kb. Now, the attachment size in all request page can be specifically given by assigning a value for a **Maximum Size of Attachment in KBs** parameter in HSS-1 tab of HRW+Application parameter screen. The parameter includes values like 100kb, 200kb, 300kb.

![C:\Users\\LENOVO\\AppData\\Local\\Temp\\SNAGHTMLefea8d6.PNG](/img/product-enhancement/image275.png)

### Print option in ESS requests for Approvers

#### July 2018 (#7163)

Print option is implemented for all Approval level users also. Approvers can print with all approver's comments after his approval. Currently this is implemented only in Leave Application and General Expense screen.

![C:\Users\\LENOVO\\AppData\\Local\\Temp\\SNAGHTMLf2f4ae9.PNG](/img/product-enhancement/image276.png)

### HSS-HLB/224- To include intermediate letters and words in proxy name filter

#### July 2018 (#10954)

The requirement here is to list out the name of the employee even when it is searched with an intermediate word wherever search functionality is implemented. This functionality works fine with strings and texts but not applicable in case of numerals as well as dropdown lists.

## Newsletter

### Attachment option in Newsletter

#### March-2019 -  #14582

Currently in Newsletter there was no option to attach any files. Thus, the client required to customise the newsletter to add the attachment.

For this,

1.  In the Newsletter Entity, add a new parameter with data type as File in Card Types.

2.  The attachment for the corresponding Newsletter could be attached in the Card Master screen which will be visible in the Newsletter Screen in ESS.

![](/img/product-enhancement/image277.png)

*Fig: Settings in the Card Master*

## AMM Module

**Accommodation Management** is a new module incorporated in ESS in order to manage and allocate camps, rooms and issue other items for employees. Some of the major screens in it are:

### Camp Master

#### July 2018  #9733

**Camp Master** is a major screen in accommodation management through which the various details like location of the camp, number of rooms included in the camp and the number of beds accommodated in each room are managed.

### Item Category

#### July 2018  #9918

**Item Category** screen will be used for defining Utilities (Uniform, Helmet, Gloves, Mattress, Pillow, Pillow Cover, Safety Shoes, Blankets, etc.). The details that would be there in the system are as follows: -

-   Category Code

-   Description

-   Frequency of Issuance: << (Lifetime, 6 Months, and 12 Months Etc.) Based on Employee Category - Staff, Labour, Driver, etc.>\>

### Item Master

#### July 2018  #9919

**Item Master** is a screen through which the various items in a category can be maintained and manipulated.

### Room Allotment

#### July 2018  #9735

**Room Allotment** is used to assign Camp/ Room / Bed to Employees. This would also handle the case of Re- Allotments / Transfer of employee from one camp/ room/ bed to another.

### Stock Receipt

#### July 2018  #9920

**Stock Receipt** is a screen through which the various stocks received by each camp are entered with effective date.

## RMM Module

Recruitment Management System includes following sub modules:

-   **Recruitment Budget Period** will be created in the HR Works in which users can create different Recruitment Budget periods, which defines the periods for which Recruitment Budgeting for recruitment are performed. The time period for Recruitment Budgeting can be specified using the fields "Start Date" and "End Date".

-   **Recruitment Budget** functionality facilitates the process of allocating Budget for various designations as a part of Recruitment in a company. Budget needs to be prepared before the commencement of a Budget Period.

-   A **Job Description** template is a master which carries various designation related information. For Example, "Job Description Template" will contain -- (a) Job Description (b) Suggested Salary (3) Qualification Required (4) Experience required, etc. In addition, it can be created for specific Location, Department etc.

-   **Manpower Requisition** enables a department to create a new vacancy request against the budget created.

-   **Publish Vacancy** is a transaction for converting approved vacancy requests into advertisement.

-   All activated vacancies from Publish Vacancy will be populated in the **Current Opening** section.

The System Process Flow of Recruitment Management System is given below:

![Activity Diagram - Rove RMS Ver 1](/img/product-enhancement/image278.png)

### Enhancement in Offer Letter print for Bosco RMM

#### Dec 2019 -  #17762

The client required the below changes in the RMM module.

-   Currently in the increment section, even if there is no increment, this line is still getting displayed in Staff offer letter which should be avoided.

-   Currently in Labor offer letter all the points are required to type manually. But the fields such as candidate name, passport no, designation and salary are only variable and all other points remain same for all labours, hence, this information is required to display automatically.

-   One-line space is compulsory after the point, 'Other Benefits'.

![](/img/product-enhancement/image279.png)

### RMM-BOSCO/30: Recruitment Manager should also be able to update standard UDF meant for Non-Recruitment Manager

#### Nov 2019 -  #17599

A new feature was added in interview assessment screen as per the client requirement.

 In interview assessment, all the standard User defined field viz -  Interviewer Comments  (or whatever been created for Non - Recruitment User) should be available to be updated by the Recruitment Manager also.

Additionally, this comment will appear in the table structure of Print of Interview assessment as per the sequence.

Further, please note if there are more than one Recruitment Manager, the entries done by the first recruitment manager in Recruitment manager related UDF should be able to seen and updated by the next Recruitment Manager as so forth.

Also, only the last updated entries in Recruitment Manager UDF should overwrite all the previously entered entries in Recruitment Manager UDF by the Recruitment Manager. 

HR Interviewer also will have Interview Status option, 'Qualified'. If the HR interviewer puts Qualified status, the candidate will remain in Assessment form for further assessment and it will not go to Candidate selection until shortlisted by another HR Interviewer.

To implement the changes, follow the steps below.

1. Execute a query to enable a new status named qualified.

![](/img/product-enhancement/image280.png)

### Multiselect option in RMM Fields in Job Description, Manpower Requisition & Auto Filtering

#### November 2019 # 17078

The client needs the following changes:

1.  Option to select multiple values for the Preferred Nationality, Preferred Language Spoken, and the Qualification Required fields in the Job Description and Manpower Requisition screen.

2.  Auto Filter option in the Sampling screen.

To implement these changes, an XML query is executed.

### Upload facility is required in Job Description

#### November 2019  #17081

The client wants an option to upload Job Description Master in bulk through excel. This is implemented by running an SQL query.

For this a template is created and users can download the excel template, fill in the fields of the excel template with corresponding data, and then upload onto the Job description master in ESS.

### Designation, Division & Department dependency in Manpower Requisition vis-a-vis Job Description

#### November 2019  #17278

The client wanted the following changes in Manpower Requisition and Job Description.

1.  The *Designation*, *Division*, *Department* and *Location* fields have to be made mandatory in the **Manpower requisition**.

2.  Although all those four fields are mandatory in Manpower requisition, In Job Description only designation will be mandatory.

**For Example**: If Sales Manager with Division = Glass is available in Job Description. And, in manpower, if any user selects (**Designation** = *Sales Manager*, **Division** = *Aluminium*), then the **Job Description** with combination of Sales Manager & Glass will be picked up.

However, if Combination of Sales Manager & Division = Glass is available, the same will be picked up in Manpower Requisition.

### Development of Cross Vacancy Functionality

#### Oct 2019 -  #16000

The client required a "Cross Vacancy" transaction in RMM Module which allow Recruitment Manager / User to apply a candidate against a vacancy different from what he initially applied for.

To implement the change, execute a query.

![](/img/product-enhancement/image281.png)

### Design Issue with  Candidate Selection 

#### Oct 2019 -  #16139

The client required to resolve the design issue in the "Candidate Selection" screen regarding the applying analytics and regenerating/ reprinting offer letter.

![](/img/product-enhancement/image282.png)

#### Development of  Bulk Upload functionality

#### Oct 2019 -  #16131

The client required the Bulk Upload functionality which allows to upload the candidate profile and their resume in Bulk. The candidate so uploaded could be uploaded to Sampling screen or Candidate Selection screens.

To implement the change, follow the steps below:

1.  Execute a query to enable bulk upload menu.

2.  Execute a query to create sequence number.

![](/img/product-enhancement/image283.png)

### Access restriction based on the usability of UDF

#### Oct 2019 -  #17158

The actual number of level of interview is decided on the run basis, for example once a candidate is through from one round of Interview, only then HR will decided if another round of interview is required , if yes - with whom. Hence, while addressing the number of the interview level, the client required the RMM system to allow the Interviewer to enter comments only in the field dedicated ( marked) for the respective person. (Viz Interviewer 1 Comments, Interviewer 2 Comments, Interviewer 3 Comments). Further, no other interviewer should be able to edit the comments of other interviewer.

Currently, in the Interview assessment section, Interviewer 1 is able to fill the comment section of Interviewer 2, & this should be restricted.  

In order to implement the change, the unwanted user defined fields (ex: interviewer 1,2,3 comments) was removed and just one field named 'Interviewer Comments' was kept.

### Access right for action in Interview Assessment

#### Oct 2019 -  #17083

In interview assessment form , the client required an option to give access ( usage) right of  Shortlist action only to certain user (such as Manager, Recruitment manager who will be sitting at the last level of Interview process) in RMM >>  Interview Assessment form. Further, the client required to deactivate  On Hold action from Interview Assessment.

![](/img/product-enhancement/image284.png)

## PMM Module

### Provision of  Request page in Interim Review & Performance Appraisal for Line Manager from Cross Company

#### Dec 2019 -  #18128

The client required to develop a provision in PMM system to have  Request page for.

1. Interim Review, and.
2. Performance Appraisal into all the company. The rights to these request page will be governed by  Roles & Users >> Program Access Right.

![](/img/product-enhancement/image285.png) 

###  Review Comment in Interim Review >> Competency Section, should not be a mandatory

#### Dec 2019 -  #17924

The client required to remove  Review Comment mandatory option from Interim Review Page (only in Competency side).

![](/img/product-enhancement/image286.png)

### Development of Interim Review Report option

#### Nov 2019 -  #13974

As per the client requirement, a new report option was developed in PMM with the name  Interim Review Report". This report will showcase and present the data of various Interim Review Transaction against the Appraisal Plan.

To implement the change, execute a query to enable the menu.

![](/img/product-enhancement/image287.png)

### Auto creation of Appraisal Plan on Plan Assignment

#### Nov 2019 -  #17303

The client required an option to create Appraisal Plan automatically on Plan Assignment for all the 'Assigned' employees.

This automatically created Appraisal Plan:

-   Should populate the default Objectives / Competencies (along with their weightages) applicable for the employee.

-   Should be in Saved mode and should be possible to further edit the same before Submitting.

-   Further, the editing of the automatically created Appraisal Plan should be possible only from the 'Proxy' tab.

### Equal Weightage' for Competencies

#### November 2019  #17311

The client has equal weightage for all competencies, and will not assign individual weightage for each competency separately.

As per the new requested change, it needs to have an option for 'Equal Weightage for Competencies'. In this option, the Weightage field associated with each competency will be 'fully hidden' in the system, and the score calculation during Performance Appraisal can be modified in 'Equal Weightage' method.

For implementing the above changes, a query needs to be executed in the database. After that in, set Competency Weightage in **HR Works >> Appraisal Plan** to ***Equal Distribution Between all Compentency (Hidden)\-\-- See the table below for*** settings in Appraisal plan:

<table class="TFtable" border="1">
    <tbody>
   <tr>
     <th>
         <p>Objective Master</p>
      </th>
      <th>
         <p>Default Button - Obj</p>
      </th>
      <th>
         <p>Default Button - Core</p>
      </th>
      <th>
         <p>Create Appraisal Plan on Plan Assignment</p>
      </th>
      <th>
         <p>Objective Weightage in Appraisal Plan</p>
      </th>
      <th>
         <p>Amendment of Default Objective</p>
      </th>
      <th>
         <p>Competency Weightage in Appraisal Plan</p>
      </th>
      <th>
         <p>Amendment of Default Competency</p>
      </th>
   </tr>
   <tr>
      <td>
         <p>Yes</p>
      </td>
      <td>
         <p>No</p>
      </td>
      <td>
         <p>No</p>
      </td>
      <td>
         <p>Yes</p>
      </td>
      <td>
         <p>Default from Objective Assignment</p>
      </td>
      <td>
         <p>Add/Edit/Delete</p>
      </td>
      <td>
         <p> Equal Distribution between all Competency (Hidden)</p>
      </td>
      <td>
         <p>Add/Edit/Delete</p>
      </td>
   </tr>
   </tbody>
</table>

![](/img/product-enhancement/image288.png)

### Auto creation of Appraisal Plan on Plan Assignment

#### November 2019  #17303

As per the changes required by the client, an option is required to create Appraisal Plan automatically on Plan Assignment for all the 'Assigned' employees.

This automatically created Appraisal Plan should have the following feature/ functionality:

-   It should populate the default Objectives / Competencies (along with their weightages) applicable for the employee.

-   It should be in Saved status and has to be editable before Submitting

-   For the client, the editing of Appraisal Plan shall be done by HR as a Proxy user, and not by the employee. Therefore, the editing of the automatically created Appraisal Plan should be possible only from the 'Proxy' tab.

To implement the above changes, a query needs to be run in the database. And the following settings has to be made in HRW >> Appraisal Plan

Set the **Create Appraisal Plan** field value to ***Create Appraisal Plan on Plan Assignment***.

![](/img/product-enhancement/image289.png)

### Replicate the enhanced features of  Interim Review &  Employee Review to  Performance Appraisal 

#### November 2019  #16691

The client needs the following changes in Performance appraisal.

1.  When a performance appraisal is submitted by the main appraiser, all objectives and competencies will be in Agree status.

2.  If the employee wants to agree with the appraisal, he/ she must provide comments in the employee can then add his/ her comments in the Comments box and then click the Agree button. The agreed records will be sent for approval and all objectives and competencies will be changed to Agree status.

3.  If the employee wants to disagree, the employee must disagree with either objectives or competencies or both.

- To enable the above functionality, ***Performance Appraisal*** has to be selected as the value of the **Employee reviewon** parameter (**HRW+ Application Parameter >> PMM-01**).

###  Employee Record Snapshot in Interim Review - Approval page

#### November 2019  #17275

The following changes have been made in the **Interim review**.

Before implementing the changes, when an employee agrees with the interim review of the reviewer, the respective record can be seen only by the first approver as per the workflow. The next level of approver can view the record only after the preceding approver approves it. Now when an employee agrees, the respective record is made visible to all levels of approvers in the Employee Record Snapshot menu.

The details will be shown in the following respective tabs in the Employee Record Snapshot once the approver/s select the corresponding employee.

1.  Employee profile

2.  Interim review

3.  Employee review
