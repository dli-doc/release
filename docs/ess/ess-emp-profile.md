# Employee Profile

## Gross Salary in Salary details

### July 2018 (#10911)

Gross salary was not shown Payroll details of Employee profile in ESS. Now this has been implemented.

![](/img/product-enhancement/image164.png)