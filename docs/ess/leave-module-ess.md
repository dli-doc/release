# Leave and Accrual

## Indemnity Scheme in the web applications

### April 2021 -  # 21444

A new screen "Indemnity scheme" was developed in web application.

In order to implement the new feature, execute the query to enable the menu.

![](/img/product-enhancement/image218.png)

## Ticket fare in the web application

### Jan 2021 -  # 21204

Developed new screen ticket fare in the web application that helps to set destination, fare and age.

In order to implement the new screen, execute a query to enable the menu.

![](/img/product-enhancement/image219.png)

## Leave master in the web application

### Jan 2021 -  # 21202

A new screen 'Leave Master' was developed in web application for creating different types of leaves.

In order to implement the change, execute a query to insert the menu.

![](/img/product-enhancement/image220.png)
