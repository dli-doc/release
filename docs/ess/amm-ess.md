# AMM Module

**Accommodation Management** is a new module incorporated in ESS in order to manage and allocate camps, rooms and issue other items for employees. Some of the major screens in it are:

## Camp Master
| Method      | Description                          |
| ----------- | ------------------------------------ |
| `GET`       | :material-check:     Fetch resource  |
| `PUT`       | :material-check-all: Update resource |
| `DELETE`    | :material-close:     Delete resource |

### July 2018  #9733

**Camp Master** is a major screen in accommodation management through which the various details like location of the camp, number of rooms included in the camp and the number of beds accommodated in each room are managed.

## Item Category

### July 2018  #9918

**Item Category** screen will be used for defining Utilities (Uniform, Helmet, Gloves, Mattress, Pillow, Pillow Cover, Safety Shoes, Blankets, etc.). The details that would be there in the system are as follows: -

-   Category Code

-   Description

-   Frequency of Issuance: << (Lifetime, 6 Months, and 12 Months Etc.) Based on Employee Category - Staff, Labour, Driver, etc.>\>

## Item Master

### July 2018  #9919

**Item Master** is a screen through which the various items in a category can be maintained and manipulated.

## Room Allotment

### July 2018  #9735

**Room Allotment** is used to assign Camp/ Room / Bed to Employees. This would also handle the case of Re- Allotments / Transfer of employee from one camp/ room/ bed to another.

## Stock Receipt

### July 2018  #9920

**Stock Receipt** is a screen through which the various stocks received by each camp are entered with effective date.
