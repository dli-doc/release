# Payroll Management

## Currency Master in Web Application

### April 2021 -  # 21199

A new screen "Currency Master" was developed in web application.

In order to implement the new feature, execute an SQL to enable the menu.

![](/img/product-enhancement/image221.png)

## Paycode Master in the web application

### Jan 2021 -  # 21201

A new screen 'Paycode Master' was developed in web application for creating different types of paycodes.

In order to implement the change, following steps were executed:

-   Execute a query to insert the menu.

-   Execute three queries to enable the module 'Pay Code Master' in ESS.

![](/img/product-enhancement/image222.png)
